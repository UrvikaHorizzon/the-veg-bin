package com.horizzon.thevegbin.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.firebase.database.core.utilities.Utilities;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.horizzon.thevegbin.R;
import com.horizzon.thevegbin.adepter.CityList_Adapter;
import com.horizzon.thevegbin.adepter.CityList_Data;
import com.horizzon.thevegbin.adepter.PincodeAdapter;
import com.horizzon.thevegbin.adepter.StateListAdapter;
import com.horizzon.thevegbin.model.Address;
import com.horizzon.thevegbin.model.Area;
import com.horizzon.thevegbin.model.AreaD;
import com.horizzon.thevegbin.model.CityListResponse;
import com.horizzon.thevegbin.model.PincodeList_Response;
import com.horizzon.thevegbin.model.Pincode_Data;
import com.horizzon.thevegbin.model.StateList_Data;
import com.horizzon.thevegbin.model.StateList_Response;
import com.horizzon.thevegbin.model.UpdateAddress;
import com.horizzon.thevegbin.model.User;
import com.horizzon.thevegbin.retrofit.APIClient;
import com.horizzon.thevegbin.retrofit.GetResult;
import com.horizzon.thevegbin.utils.SessionManager;
import com.horizzon.thevegbin.utils.Utiles;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.horizzon.thevegbin.utils.Utiles.isRef;

public class AddressActivity extends BaseActivity implements GetResult.MyListener, GoogleApiClient.OnConnectionFailedListener {
    @BindView(R.id.ed_username)
    EditText edUsername;
    @BindView(R.id.ed_type)
    EditText edType;
    @BindView(R.id.state_spinner)
    Spinner state_spinner;
    @BindView(R.id.city_spinner)
    Spinner city_spinner;
   /* @BindView(R.id.ed_landmark)
    EditText edLandmark;*/
    SessionManager sessionManager;
    @BindView(R.id.ed_hoousno)
    EditText edHoousno;
    @BindView(R.id.ed_society)
    EditText edSociety;
    @BindView(R.id.ed_pinno)
    Spinner edPinno;
 /*   @BindView(R.id.location)
    EditText edlocation;*/
    @BindView(R.id.address)
    EditText edaddress;
    String areaSelect;
    List<AreaD> areaDS = new ArrayList<>();
    @BindView(R.id.spinner)
    Spinner spinner;
    User user;
    Address address;
    int PLACE_PICKER_REQUEST = 1463;
    String latitude, longitude;

    List<String> categories = new ArrayList<String>();
    //    private List<AddressTypeModel> values = new ArrayList<>();
    List<StateList_Data> state_list = new ArrayList<>();
    List<CityList_Data> citylist;
    // List<AreaList_Data> arealist;
    StateListAdapter stateListAdapter;
    // AddressCategoryTypeAdapter addressCategoryTypeAdapter;
    CityList_Adapter cityList_adapter;
    //  AreaListAdapter areaListAdapter;

    int stateId, CityId,pincodeId;
    String state_name = "", city_name = "", area_name = "";
    String state_id = "", city_id = "", area_id = "";
    SharedPreferences sharedPreferences;
    int selectedMathod;
    String AddressType;
    String pincode_id = "";
    String pincode_name = "";
    List<Pincode_Data> pincode_list = new ArrayList<>();
    PincodeAdapter pincodeAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_address);
        ButterKnife.bind(this);
        getstateList();
        getpincode();
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        sessionManager = new SessionManager(AddressActivity.this);
        user = sessionManager.getUserDetails("");
        address = (Address) getIntent().getSerializableExtra("MyClass");
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                areaSelect = areaDS.get(position).getName();
                Log.d("TAG", "onItemSelected: area:" + areaSelect);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        GetArea();
        if (address != null)
            setcountaint(address);

        citylist = new ArrayList<>();

        state_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view,
                                       int position, long id) {
                // Here you get the current item (a User object) that is selected by its position
                StateList_Data data = stateListAdapter.getItem(position);
                // Here you can do the action you want to...
                state_id = data.getId();
                state_name = data.getStatename();
                sessionManager.setStateId(state_id, state_name);
                getCityList(state_id);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapter) {
            }
        });


        city_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                CityList_Data data = cityList_adapter.getItem(position);
                city_id = data.getId();
                city_name = data.getCityname();
                sessionManager.setCityId(city_id, city_name);
                //  getAreaList(sessionManager.getStateId(), city_id);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        edPinno.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view,
                                       int position, long id) {
                // Here you get the current item (a User object) that is selected by its position
                Pincode_Data data = pincodeAdapter.getItem(position);
                // Here you can do the action you want to...
                pincode_id = data.getId();
                pincode_name = data.getPncodename();
                sessionManager.setPincodeId(pincode_id, pincode_name);
               // getCityList(pincode_id);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapter) {
            }
        });

        /*edlocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent autocompleteIntent = new Intent(getApplicationContext(), MapActivity.class);
                //requestCode in INT
                startActivityForResult(autocompleteIntent, PLACE_PICKER_REQUEST);
            }
        });*/

    }


    @Override
    protected void onResume() {
        super.onResume();
        sharedPreferences.registerOnSharedPreferenceChangeListener(sharedPreferenceChangeListener);
    }

    private void setcountaint(Address address) {
        edUsername.setText("" + address.getName());
        edType.setText("" + address.getName());
        edHoousno.setText("" + address.getHno());
        edSociety.setText("" + address.getSociety());
       // edPinno.setText("" + address.getPincode());
       // edLandmark.setText("" + address.getLandmark());
        edType.setText("" + address.getType());
        //  stateId = Integer.parseInt(address.getState());
        //  CityId = Integer.parseInt(address.getCity());
        edaddress.setText(""+ address.getAddress());
        Log.d("TAG", "city: " + address.getCity());
        Log.d("TAG", "state: " + address.getState());
    }

    private void GetArea() {
        JSONObject jsonObject = new JSONObject();
        JsonParser jsonParser = new JsonParser();
        Call<JsonObject> call = APIClient.getInterface().getArea((JsonObject) jsonParser.parse(jsonObject.toString()));
        GetResult getResult = new GetResult();
        getResult.setMyListener(this);
        getResult.callForLogin(call, "2");
    }

    @OnClick(R.id.txt_save)
    public void onViewClicked() {
        if (validation()) {
            if (address != null) {
                UpdateUser(address.getId());
            } else {
                UpdateUser("0");
            }
        }
    }

    private void UpdateUser(String aid) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("uid", user.getId());
            jsonObject.put("aid", aid);
            jsonObject.put("name", edUsername.getText().toString());
            jsonObject.put("hno", edHoousno.getText().toString());
            jsonObject.put("society", edSociety.getText().toString());
            jsonObject.put("area", areaSelect);
            jsonObject.put("city_id", sessionManager.getCityId());
            jsonObject.put("state_id", sessionManager.getStateId());
           // jsonObject.put("landmark", edLandmark.getText().toString());
            jsonObject.put("address", edaddress.getText().toString());
            jsonObject.put("pincode", sessionManager.getPincodeId());
            jsonObject.put("type", edType.getText().toString());
            jsonObject.put("mobile", user.getMobile());
            jsonObject.put("password", user.getPassword());
            jsonObject.put("imei", Utiles.getIMEI(AddressActivity.this));
            JsonParser jsonParser = new JsonParser();
            Call<JsonObject> call = APIClient.getInterface().UpdateAddress((JsonObject) jsonParser.parse(jsonObject.toString()));
            GetResult getResult = new GetResult();
            getResult.setMyListener(this);
            getResult.callForLogin(call, "1");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void callback(JsonObject result, String callNo) {
        try {
            if (callNo.equalsIgnoreCase("1")) {
                Gson gson = new Gson();
                UpdateAddress response = gson.fromJson(result.toString(), UpdateAddress.class);
                Toast.makeText(AddressActivity.this, "" + response.getResponseMsg(), Toast.LENGTH_LONG).show();
                if (response.getResult().equals("true")) {
                    sessionManager.setAddress("", response.getAddress());
                    isRef = true;
                    finish();
                }
            } else if (callNo.equalsIgnoreCase("2")) {
                Gson gson = new Gson();
                Area area = gson.fromJson(result.toString(), Area.class);
                areaDS = area.getData();
                List<String> Arealist = new ArrayList<>();
                for (int i = 0; i < areaDS.size(); i++) {
                    if (areaDS.get(i).getStatus().equalsIgnoreCase("1")) {
                        Arealist.add(areaDS.get(i).getName());
                    }
                }
                ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, Arealist);
                dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinner.setAdapter(dataAdapter);
                int spinnerPosition = dataAdapter.getPosition(address.getArea());
                spinner.setSelection(spinnerPosition);
            }
        } catch (Exception e) {
        }
    }

    public boolean validation() {
        if (edUsername.getText().toString().isEmpty()) {
            edUsername.setError("Enter Name");
            return false;
        }
        if (edHoousno.getText().toString().isEmpty()) {
            edHoousno.setError("Enter House No");
            return false;
        }
        if (edSociety.getText().toString().isEmpty()) {
            edSociety.setError("Enter Society");
            return false;
        }
       /* if (edLandmark.getText().toString().isEmpty()) {
            edLandmark.setError("Enter Landmark");
            return false;
        }*/
        /*if (edPinno.getText().toString().isEmpty()) {
            edPinno.setError("Enter Pincode");
            return false;
        }*/
        if (spinner != null && spinner.getSelectedItem() != null) {
        } else {
            Toast.makeText(this, "area name list is not available;", Toast.LENGTH_SHORT).show();
        }
        if (state_spinner != null && state_spinner.getSelectedItem() != null) {
        } else {
            Toast.makeText(this, "state name is not insert", Toast.LENGTH_SHORT).show();
        }

        if (city_spinner != null && city_spinner.getSelectedItem() != null) {
        } else {
            Toast.makeText(this, "city name not insert", Toast.LENGTH_SHORT).show();
        }
        if (edaddress.getText().toString().isEmpty()) {
            edaddress.setError("Invalid Address");
            return false;
        }
        if (edPinno != null && edPinno.getSelectedItem() != null) {
        } else {
            Toast.makeText(this, "Pincode not insert", Toast.LENGTH_SHORT).show();
        }
        return true;
    }

    private static boolean validatePhoneNumber(String phoneNo) {
        //validate phone numbers of format "1234567890"
        if (phoneNo.matches("\\d{10}")) return true;
            //validating phone number with -, . or spaces
        else if (phoneNo.matches("\\d{3}[-\\.\\s]\\d{3}[-\\.\\s]\\d{4}")) return true;
            //validating phone number with extension length from 3 to 5
        else if (phoneNo.matches("\\d{3}-\\d{3}-\\d{4}\\s(x|(ext))\\d{3,5}")) return true;
            //validating phone number where area code is in braces ()
        else if (phoneNo.matches("\\(\\d{3}\\)-\\d{3}-\\d{4}")) return true;
            //return false if nothing matches the input
        else return false;
    }


    /*@Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {
                String lat = data.getStringExtra("lat");
                String lon = data.getStringExtra("lon");
                latitude = lat;
                longitude = lon;
                String address = Utiles.getCompleteAddressString(getApplicationContext(), Double.parseDouble(latitude), Double.parseDouble(longitude));
          //      edlocation.setText(latitude + ", " + longitude);
                edaddress.setText(address);
            } else {
                Log.e("ShippingAddress", "Error in data fetching");
                // Toast.makeText(getActivity(),   "Error in data fetching", Toast.LENGTH_SHORT).show();
            }

        }
    }*/

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Toast.makeText(getApplicationContext(), connectionResult.getErrorMessage() + "", Toast.LENGTH_SHORT).show();
    }

    private void getCityList(String id) {

        Call<CityListResponse> call = APIClient.getInterface().getCityList(id);
        call.enqueue(new Callback<CityListResponse>() {
            @Override
            public void onResponse(Call<CityListResponse> call, Response<CityListResponse> response) {
                if (response.isSuccessful()) {


                    if (response.body().getResponseCode().equals("200")) {

                        citylist = response.body().getData();
                        cityList_adapter = new CityList_Adapter(AddressActivity.this, android.R.layout.simple_spinner_item, citylist);
                        if (CityId > 0) {
                            for (int i = 0; i < citylist.size(); i++) {
                                if (CityId == Integer.parseInt(citylist.get(i).getId())) {
                                    city_spinner.setSelection(i);
                                }
                            }
                        }

                        city_spinner.setAdapter(cityList_adapter);

                    } else if (response.body().getResponseCode().equals("401")) {
                        citylist.clear();
                        citylist = new ArrayList<>();
                        // getAreaList(state_id, city_id);
                        cityList_adapter = new CityList_Adapter(AddressActivity.this, android.R.layout.simple_spinner_item, citylist);
                        city_spinner.setAdapter(cityList_adapter);
                        cityList_adapter.notifyDataSetChanged();
                        Toast.makeText(AddressActivity.this, "no Data Found", Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<CityListResponse> call, Throwable t) {

            }
        });
    }

    private void getstateList() {
        Call<StateList_Response> call = APIClient.getInterface().getStateList();
        call.enqueue(new Callback<StateList_Response>() {
            @Override
            public void onResponse(Call<StateList_Response> call, Response<StateList_Response> response) {
                if (response.isSuccessful()) {


                    if (response.body().getResponseCode().equals("200")) {
                        state_list = response.body().getData();
                        stateListAdapter = new StateListAdapter(AddressActivity.this,
                                android.R.layout.simple_spinner_item,
                                state_list);
                        state_spinner.setAdapter(stateListAdapter);

                        for (int i = 0; i < state_list.size(); i++) {
                            if (Integer.parseInt(state_list.get(i).getId()) == stateId) {
                                state_spinner.setId(i);
                                state_spinner.setSelection(i);
                            }
                        }
                        stateListAdapter.notifyDataSetChanged(); // Set the custom adapter to the spinner
                    } else if (response.body().getResponseCode().equals("401")) {
                        state_list.clear();
                        stateListAdapter = new StateListAdapter(AddressActivity.this,
                                android.R.layout.simple_spinner_item,
                                state_list);
                        state_spinner.setAdapter(stateListAdapter);
                        stateListAdapter.notifyDataSetChanged();
                        Toast.makeText(AddressActivity.this, "no Data Found", Toast.LENGTH_SHORT).show();

                    }

                }
            }

            @Override
            public void onFailure(Call<StateList_Response> call, Throwable t) {

            }
        });


        // You can create an anonymous listener to handle the event when is selected an spinner item


    }

    private void getpincode() {
        Call<PincodeList_Response> call = APIClient.getInterface().getPincodelist();
        call.enqueue(new Callback<PincodeList_Response>() {
            @Override
            public void onResponse(Call<PincodeList_Response> call, Response<PincodeList_Response> response) {
                if (response.isSuccessful()) {


                    if (response.body().getResponseCode().equals("200")) {
                        pincode_list = response.body().getData();
                        pincodeAdapter = new PincodeAdapter(AddressActivity.this,
                                android.R.layout.simple_spinner_item,
                                pincode_list);
                        edPinno.setAdapter(pincodeAdapter);

                        for (int i = 0; i < pincode_list.size(); i++) {
                            if (Integer.parseInt(pincode_list.get(i).getId()) == pincodeId) {
                                state_spinner.setId(i);
                                state_spinner.setSelection(i);
                            }
                        }
                        pincodeAdapter.notifyDataSetChanged(); // Set the custom adapter to the spinner
                    } else if (response.body().getResponseCode().equals("401")) {
                        pincode_list.clear();
                        pincodeAdapter = new PincodeAdapter(AddressActivity.this,
                                android.R.layout.simple_spinner_item,
                                pincode_list);
                        edPinno.setAdapter(pincodeAdapter);
                        pincodeAdapter.notifyDataSetChanged();
                        Toast.makeText(AddressActivity.this, "no Data Found", Toast.LENGTH_SHORT).show();

                    }

                }
            }

            @Override
            public void onFailure(Call<PincodeList_Response> call, Throwable t) {

            }
        });


        // You can create an anonymous listener to handle the event when is selected an spinner item


    }

    SharedPreferences.OnSharedPreferenceChangeListener sharedPreferenceChangeListener = new SharedPreferences.OnSharedPreferenceChangeListener() {
        @Override
        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
            if (key.equals("area_name")) {
                // Write your code here
                area_name = sharedPreferences.getString("area_name", "");
            }
        }
    };

}
