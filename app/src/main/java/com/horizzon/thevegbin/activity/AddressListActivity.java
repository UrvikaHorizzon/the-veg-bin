package com.horizzon.thevegbin.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.horizzon.thevegbin.R;
import com.horizzon.thevegbin.model.Address;
import com.horizzon.thevegbin.model.AddressData;
import com.horizzon.thevegbin.model.DairyItem;
import com.horizzon.thevegbin.model.Price;
import com.horizzon.thevegbin.model.RestResponse;
import com.horizzon.thevegbin.model.Subscribe;
import com.horizzon.thevegbin.model.User;
import com.horizzon.thevegbin.retrofit.APIClient;
import com.horizzon.thevegbin.retrofit.GetResult;
import com.horizzon.thevegbin.utils.CustPrograssbar;
import com.horizzon.thevegbin.utils.SessionManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;

import static com.horizzon.thevegbin.activity.SubscribeActivity.subscribactivity;
import static com.horizzon.thevegbin.utils.SessionManager.ADDRESS1;
import static com.horizzon.thevegbin.utils.Utiles.isRef;
import static com.horizzon.thevegbin.utils.Utiles.isSelect;
import static com.horizzon.thevegbin.utils.Utiles.seletAddress;

public class AddressListActivity extends AppCompatActivity implements GetResult.MyListener {

    DairyItem productItem;
    ArrayList<Price> priceslist;
    @BindView(R.id.txt_notfound)
    TextView txtNotfound;
    @BindView(R.id.lvl_notfound)
    LinearLayout lvlNotfound;
    @BindView(R.id.recycle_address_activity)
    RecyclerView recycleAddress;
    @BindView(R.id.btn_addaddress)
    TextView btnAddaddress;
    User user;
    SessionManager sessionManager;
    CustPrograssbar custPrograssbar;
    int positionAdd;
    List<Address> addressList;
    SelectAdrsAdapter adapter;
    List<DairyItem> dairyDataList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_address_list);
        ButterKnife.bind(this);
        sessionManager = new SessionManager(this);
        custPrograssbar = new CustPrograssbar();
        user = new User();
        user = sessionManager.getUserDetails("");
        Intent getintent = getIntent();
        productItem = getintent.getParcelableExtra("MyClass");
        priceslist = getIntent().getParcelableArrayListExtra("MyList");
        recycleAddress.setLayoutManager(new GridLayoutManager(getApplicationContext(), 1));
        LinearLayoutManager mLayoutManager1 = new LinearLayoutManager(getApplicationContext());
        mLayoutManager1.setOrientation(LinearLayoutManager.HORIZONTAL);
        getAddress();
    }

    @OnClick(R.id.btn_addaddress)
    public void onViewClicked() {
        isRef = false;
        startActivity(new Intent(getApplicationContext(), AdrsSubscribeActivity.class));
    }

    public class SelectAdrsAdapter extends
            RecyclerView.Adapter<SelectAdrsAdapter.ViewHolder> {
        private List<Address> addressList;

        private int lastSelectedPosition = -1;

        public SelectAdrsAdapter(List<Address> addressList) {
            this.addressList = addressList;
        }

        @Override
        public SelectAdrsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                               int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.selectaddress_item, parent, false);
            SelectAdrsAdapter.ViewHolder viewHolder =
                    new SelectAdrsAdapter.ViewHolder(view);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(SelectAdrsAdapter.ViewHolder holder,
                                     int position) {
            Address address = addressList.get(position);
            holder.offerSelect.setChecked(lastSelectedPosition == position);
            holder.txtAddressful.setText(address.getHno() + "," + address.getSociety() + "," + address.getArea() + ","  + address.getName());
            holder.txtAddressname.setText("" + address.getType());
            if (!isSelect) {
                holder.offerSelect.setVisibility(View.GONE);
            }
        }

        @Override
        public int getItemCount() {
            return addressList.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            @BindView(R.id.offer_select)
            RadioButton offerSelect;
            @BindView(R.id.txt_addressname)
            TextView txtAddressname;
            @BindView(R.id.txt_addressful)
            TextView txtAddressful;
            @BindView(R.id.txt_change)
            ImageView txtChange;
            @BindView(R.id.txt_delete)
            ImageView txtDelete;

            public ViewHolder(View view) {
                super(view);
                ButterKnife.bind(this, view);
                offerSelect.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        lastSelectedPosition = getAdapterPosition();
                        seletAddress = lastSelectedPosition;
                        sessionManager.setAddress(ADDRESS1, addressList.get(getAdapterPosition()));
                        startActivity(new Intent(AddressListActivity.this, SubscribeActivity.class).putExtra("MyClass", productItem).putParcelableArrayListExtra("MyList", priceslist));
                    }
                });
                txtDelete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        lastSelectedPosition = getAdapterPosition();
                        positionAdd = lastSelectedPosition;
                        DeleteAddress(addressList.get(lastSelectedPosition).getId());
                    }
                });
                txtChange.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        startActivity(new Intent(getApplicationContext(), AdrsSubscribeActivity.class).putExtra("MyClass", addressList.get(getAdapterPosition())));
                    }
                });
            }
        }
    }

    private void DeleteAddress(String aid) {
        custPrograssbar.PrograssCreate(getApplicationContext());
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("uid", user.getId());
            jsonObject.put("aid", aid);

            JsonParser jsonParser = new JsonParser();
            Call<JsonObject> call = APIClient.getInterface().deleteAddress((JsonObject) jsonParser.parse(jsonObject.toString()));
            GetResult getResult = new GetResult();
            getResult.setMyListener(this);
            getResult.callForLogin(call, "2");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void getAddress() {
        custPrograssbar.PrograssCreate(getApplicationContext());
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("uid", user.getId());
            JsonParser jsonParser = new JsonParser();
            Call<JsonObject> call = APIClient.getInterface().getAddress((JsonObject) jsonParser.parse(jsonObject.toString()));
            GetResult getResult = new GetResult();
            getResult.setMyListener(this);
            getResult.callForLogin(call, "address");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void callback(JsonObject result, String callNo) {
        custPrograssbar.ClosePrograssBar();
        if (callNo.equalsIgnoreCase("address")) {
            Gson gson = new Gson();
            try {
                AddressData addressData = gson.fromJson(result.toString(), AddressData.class);
                if (addressData.getResult().equalsIgnoreCase("true")) {
                    if (addressData.getResultData().size() != 0) {
                        lvlNotfound.setVisibility(View.GONE);

                        addressList = addressData.getResultData();
                        adapter = new SelectAdrsAdapter(addressList);
                        recycleAddress.setAdapter(adapter);
                    }
                } else {
                    lvlNotfound.setVisibility(View.VISIBLE);
                    txtNotfound.setText("" + addressData.getResponseMsg());
                }
            } catch (Exception e) {
                e.toString();
            }
        } else if (callNo.equalsIgnoreCase("2")) {
            try {
                Gson gson = new Gson();
                RestResponse restResponse = gson.fromJson(result.toString(), RestResponse.class);
                Toast.makeText(getApplicationContext(), restResponse.getResponseMsg(), Toast.LENGTH_SHORT).show();
                if (restResponse.getResult().equalsIgnoreCase("true")) {
                    addressList.remove(positionAdd);
                    if (addressList.size() == 0) {
                        lvlNotfound.setVisibility(View.VISIBLE);
                        txtNotfound.setText("Address Not Found!!");
                    }
                    adapter.notifyDataSetChanged();
                }
            } catch (Exception e) {
                e.toString();
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    //    HomeActivity.getInstance().serchviewHide();
   //     HomeActivity.getInstance().setFrameMargin(0);
        if (isRef) {
            isRef = false;
            getAddress();
        }
    }
}
