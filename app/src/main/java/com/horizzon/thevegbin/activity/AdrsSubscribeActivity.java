package com.horizzon.thevegbin.activity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.horizzon.thevegbin.R;
import com.horizzon.thevegbin.adepter.CityList_Data;
import com.horizzon.thevegbin.adepter.StateListAdapter;
import com.horizzon.thevegbin.model.Address;
import com.horizzon.thevegbin.model.Area;
import com.horizzon.thevegbin.model.AreaD;
import com.horizzon.thevegbin.model.StateList_Data;
import com.horizzon.thevegbin.model.StateList_Response;
import com.horizzon.thevegbin.model.UpdateAddress;
import com.horizzon.thevegbin.model.User;
import com.horizzon.thevegbin.retrofit.APIClient;
import com.horizzon.thevegbin.retrofit.GetResult;
import com.horizzon.thevegbin.utils.SessionManager;
import com.horizzon.thevegbin.utils.Utiles;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.horizzon.thevegbin.utils.Utiles.isRef;

public class AdrsSubscribeActivity extends AppCompatActivity implements GetResult.MyListener {

    @BindView(R.id.ed_username_sub)
    EditText edUsername;
    @BindView(R.id.ed_type_sub)
    EditText edType;
    @BindView(R.id.ed_society_sub)
    EditText edSociety;
    @BindView(R.id.spinner_sub)
    Spinner spinner;

    SessionManager sessionManager;
    SharedPreferences sharedPreferences;
    User user;
    Address address;
    String areaSelect;
    List<AreaD> areaDS = new ArrayList<>();;
    String state_id = "";
    String state_name = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adrs_subscribe);
        ButterKnife.bind(this);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        sessionManager = new SessionManager(AdrsSubscribeActivity.this);
        user = sessionManager.getUserDetails("");
        address = (Address) getIntent().getSerializableExtra("MyClass");
        areaDS = new ArrayList<>();

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                areaSelect = areaDS.get(position).getName();
                Log.d("TAG", "onItemSelected: area:" + areaSelect);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        GetArea();
    }

    private void GetArea() {
        JSONObject jsonObject = new JSONObject();
        JsonParser jsonParser = new JsonParser();
        Call<JsonObject> call = APIClient.getInterface().getArea((JsonObject) jsonParser.parse(jsonObject.toString()));
        GetResult getResult = new GetResult();
        getResult.setMyListener(this);
        getResult.callForLogin(call, "2");
    }

    @OnClick(R.id.txt_save_sub)
    public void onViewClicked() {
        if (validation()) {
            if (address != null) {
                UpdateUser(address.getId());
            } else {
                UpdateUser("0");
            }
        }
    }

    private void UpdateUser(String aid) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("uid", user.getId());
            jsonObject.put("aid", aid);
            jsonObject.put("name", edUsername.getText().toString());
            jsonObject.put("hno", edType.getText().toString());
            jsonObject.put("society", edSociety.getText().toString());
            jsonObject.put("area", areaSelect);
            jsonObject.put("city_id", "1");
            jsonObject.put("state_id", "1");
            // jsonObject.put("landmark", edLandmark.getText().toString());
            jsonObject.put("address", "1");
            jsonObject.put("pincode", "1");
            jsonObject.put("type", "1");
            jsonObject.put("mobile", "1");
            jsonObject.put("password", "1");
            jsonObject.put("imei", Utiles.getIMEI(AdrsSubscribeActivity.this));
            JsonParser jsonParser = new JsonParser();
            Call<JsonObject> call = APIClient.getInterface().UpdateAddress((JsonObject) jsonParser.parse(jsonObject.toString()));
            GetResult getResult = new GetResult();
            getResult.setMyListener(this);
            getResult.callForLogin(call, "1");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void callback(JsonObject result, String callNo) {
        try {
            if (callNo.equalsIgnoreCase("1")) {
                Gson gson = new Gson();
                UpdateAddress response = gson.fromJson(result.toString(), UpdateAddress.class);
                Toast.makeText(AdrsSubscribeActivity.this, "" + response.getResponseMsg(), Toast.LENGTH_LONG).show();
                if (response.getResult().equals("true")) {
                    sessionManager.setAddress("", response.getAddress());
                    isRef = true;
                    finish();
                }
            } else if (callNo.equalsIgnoreCase("2")) {
                Gson gson = new Gson();
                Area area = gson.fromJson(result.toString(), Area.class);
                areaDS = area.getData();
                List<String> Arealist = new ArrayList<>();
                for (int i = 0; i < areaDS.size(); i++) {
                if (areaDS.get(i).getSubscribe().equalsIgnoreCase("Yes")) {

                    if (areaDS.get(i).getStatus().equalsIgnoreCase("1")) {
                            Arealist.add(areaDS.get(i).getName());
                        }
                    }
                }
                ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, Arealist);
                dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinner.setAdapter(dataAdapter);
                int spinnerPosition = dataAdapter.getPosition(address.getArea());
                spinner.setSelection(spinnerPosition);
            }
        } catch (Exception e) {
        }
    }

    public boolean validation() {
        if (edUsername.getText().toString().isEmpty()) {
            edUsername.setError("Enter Name");
            return false;
        }
        if (edType.getText().toString().isEmpty()) {
            edType.setError("Enter Block No");
            return false;
        }
        if (edSociety.getText().toString().isEmpty()) {
            edSociety.setError("Enter Society Name");
            return false;
        }
        if (spinner != null && spinner.getSelectedItem() != null) {
        } else {
            Toast.makeText(this, "area name list is not available;", Toast.LENGTH_SHORT).show();
        }
        return true;
    }


}