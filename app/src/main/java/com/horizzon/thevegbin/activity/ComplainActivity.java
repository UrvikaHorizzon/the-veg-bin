package com.horizzon.thevegbin.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.horizzon.thevegbin.R;
import com.horizzon.thevegbin.model.Promocode_Response;
import com.horizzon.thevegbin.model.Promocode_Response_Data;
import com.horizzon.thevegbin.model.UpdateAddress;
import com.horizzon.thevegbin.model.User;
import com.horizzon.thevegbin.retrofit.APIClient;
import com.horizzon.thevegbin.retrofit.GetResult;
import com.horizzon.thevegbin.utils.CustPrograssbar;
import com.horizzon.thevegbin.utils.SessionManager;
import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.horizzon.thevegbin.utils.SessionManager.CURRUNCY;
import static com.horizzon.thevegbin.utils.Utiles.isRef;


public class ComplainActivity extends BaseActivity implements GetResult.MyListener {

    @BindView(R.id.ed_username_compl)
    EditText edUsername_compl;
    @BindView(R.id.ed_complain)
    TextInputEditText edcomplain;
    @BindView(R.id.btn_complain)
    TextView btnSendCompl;
    User user;
    SessionManager sessionManager;
    CustPrograssbar custPrograssbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_complain);
        ButterKnife.bind(this);
        sessionManager = new SessionManager(ComplainActivity.this);
        user = sessionManager.getUserDetails("");
        custPrograssbar = new CustPrograssbar();
        edUsername_compl.setText("" + user.getName());
    }
    private void UserFeedBack() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("user_id", user.getId());
            jsonObject.put("name", edUsername_compl.getText().toString());
            jsonObject.put("complain", edcomplain.getText().toString());
            JsonParser jsonParser = new JsonParser();
            Call<JsonObject> call = APIClient.getInterface().getComplain((JsonObject) jsonParser.parse(jsonObject.toString()));
            GetResult getResult = new GetResult();
            getResult.setMyListener(this);
            getResult.callForLogin(call, "1");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    @Override
    public void callback(JsonObject result, String callNo) {
        try {
            if (callNo.equalsIgnoreCase("1")) {
                Gson gson = new Gson();
                UpdateAddress response = gson.fromJson(result.toString(), UpdateAddress.class);
                Toast.makeText(ComplainActivity.this, "" + response.getResponseMsg(), Toast.LENGTH_LONG).show();
                if (response.getResult().equals("true")) {
                    // sessionManager.setAddress("", response.getAddress());
                    isRef = true;
                    finish();
                }
            } else if (callNo.equalsIgnoreCase("2")) {

            }
        } catch (Exception e) {
        }
    }
    @OnClick(R.id.btn_complain)
    public void onViewClicked() {
        if (validation()) {
            UserFeedBack();
        }
    }
    private boolean validation() {
        if (edUsername_compl.getText().toString().isEmpty()) {
            edUsername_compl.setError("Enter Name");
            return false;
        }
        if (edcomplain.getText().toString().isEmpty()) {
            edcomplain.setError("Enter Complain");
            return false;
        }
        return true;
    }
}
