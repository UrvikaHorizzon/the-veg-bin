package com.horizzon.thevegbin.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.horizzon.thevegbin.R;

public class CustomDialog extends Dialog {

    Activity context;
    private ImageView ivClose,ivoffer;
    private TextView ivtitle,ivcontent;
    private Button ok;
    public CustomDialog(Context context) {
        super(context);
        this.context = (Activity) context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.custom_dialog);

      //  ivClose = (ImageView) findViewById(R.id.ivClose);
     //   ivtitle = (TextView) findViewById(R.id.ivtitle);
      //  ivoffer = (ImageView) findViewById(R.id.ivOffer);
     //   ivcontent = (TextView) findViewById(R.id.tvContent);
     //   ok = (Button) findViewById(R.id.btnok);
        ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
        ivtitle.setText("Hello I Am Nikul");
   //     ivoffer.setImageResource(R.drawable.amulcowghee);
        ivcontent.setText("Milk is a nutrient-rich liquid food produced in the mammary glands of mammals. It is the primary source of nutrition for infant mammals (including humans who are breastfed) before they are able to digest other types of food. ... It contains many other nutrients including protein and lactose................Milk is a nutrient-rich liquid food produced in the mammary glands of mammals. It is the primary source of nutrition for infant mammals (including humans who are breastfed) before they are able to digest other types of food. ... It contains many other nutrients including protein and lactose.");

        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

    }

    public void setArguments() {
    }

    public void setArguments(Bundle args) {
    }
}
