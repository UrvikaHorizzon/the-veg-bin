package com.horizzon.thevegbin.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.widget.Toast;

import com.horizzon.thevegbin.R;
import com.horizzon.thevegbin.adepter.HistoryAdapter;
import com.horizzon.thevegbin.model.History_Response;
import com.horizzon.thevegbin.model.Response_Data;
import com.horizzon.thevegbin.model.User;
import com.horizzon.thevegbin.retrofit.APIClient;
import com.horizzon.thevegbin.utils.CustPrograssbar;
import com.horizzon.thevegbin.utils.SessionManager;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HistoryActivity extends AppCompatActivity {

    @BindView(R.id.recycle_history)
    RecyclerView rvrecycle;

    List<Response_Data> historyList = new ArrayList<>();
    HistoryAdapter historyadapter;
    User user;
    SessionManager sessionManager;
    CustPrograssbar custPrograssbar;
    String rs;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);
        ButterKnife.bind(this);
        sessionManager = new SessionManager(this);
        user = sessionManager.getUserDetails("");
        custPrograssbar = new CustPrograssbar();
        rs = user.getId();

        rvrecycle.setLayoutManager(new GridLayoutManager(getApplicationContext(), 1));
        LinearLayoutManager mLayoutManager1 = new LinearLayoutManager(getApplicationContext());
        mLayoutManager1.setOrientation(LinearLayoutManager.HORIZONTAL);

        getWallethistory(rs);
    }

    private void getWallethistory(String id) {

        Call<History_Response> call = APIClient.getInterface().getWalletHistory(id);
        call.enqueue(new Callback<History_Response>() {
            @Override
            public void onResponse(Call<History_Response> call, Response<History_Response> response) {
                if (response.isSuccessful()) {


                    if (response.body().getResponseCode().equals("200")) {
                       /* historyList.addAll(response.body().getDetail());
                        rvrecycle.setAdapter(historyadapter);*/

                        History_Response jsonResponse = response.body();
                        historyList = new ArrayList<Response_Data>(jsonResponse.getDetail());
                        historyadapter = new HistoryAdapter(historyList);
                        rvrecycle.setAdapter(historyadapter);

                    } else if (response.body().getResponseCode().equals("401")) {

                        Toast.makeText(HistoryActivity.this, "no Data Found", Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<History_Response> call, Throwable t) {

            }
        });
    }


}