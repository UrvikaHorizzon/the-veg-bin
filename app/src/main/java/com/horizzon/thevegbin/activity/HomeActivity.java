package com.horizzon.thevegbin.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.horizzon.thevegbin.R;
import com.horizzon.thevegbin.database.DatabaseHelper;
import com.horizzon.thevegbin.fragment.AddressFragment;
import com.horizzon.thevegbin.fragment.CardFragment;
import com.horizzon.thevegbin.fragment.HomeFragment;
import com.horizzon.thevegbin.fragment.ItemListFragment;
import com.horizzon.thevegbin.fragment.MyOrderFragment;
import com.horizzon.thevegbin.fragment.OrderSumrryFragment;
import com.horizzon.thevegbin.fragment.SubCategoryFragment;
import com.horizzon.thevegbin.fragment.SubscribeFragment;
import com.horizzon.thevegbin.fragment.SubscriptionListFragment;
import com.horizzon.thevegbin.model.User;
import com.horizzon.thevegbin.utils.CustPrograssbar;
import com.horizzon.thevegbin.utils.SessionManager;
import com.google.android.material.appbar.AppBarLayout;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.horizzon.thevegbin.fragment.ItemListFragment.itemListFragment;
import static com.horizzon.thevegbin.fragment.OrderSumrryFragment.ISORDER;
import static com.horizzon.thevegbin.utils.SessionManager.LOGIN;
import static com.horizzon.thevegbin.utils.Utiles.isSelect;


public class HomeActivity extends AppCompatActivity {
 //   @BindView(R.id.ed_search)
//    EditText edSearch;
  //  @BindView(R.id.img_search)
  //  ImageView imgSearch;
    @BindView(R.id.img_cart)
    ImageView imgCart;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;
 //   @BindView(R.id.lvl_actionsearch)
 //   LinearLayout lvlActionsearch;
    @BindView(R.id.lvl_home)
    LinearLayout lvlHome;
    @BindView(R.id.lvl_mainhome)
    LinearLayout lvlMainhome;
    @BindView(R.id.txt_actiontitle)
    TextView txtActiontitle;
    @BindView(R.id.txt_logintitel)
    TextView txtLogintitel;
    @BindView(R.id.fragment_frame)
    FrameLayout fragmentFrame;
    @BindView(R.id.appBarLayout)
    AppBarLayout appBarLayout;
  //  @BindView(R.id.lvl_hidecart)
  //  LinearLayout lvlHidecart;
    @BindView(R.id.txt_mob)
    TextView txtMob;
//    @BindView(R.id.txtfirstl)
 //   TextView txtfirstl;
 //   @BindView(R.id.txt_name)
 //   TextView txtName;
 //   @BindView(R.id.txt_email)
 //   TextView txtEmail;
    @BindView(R.id.app_verdisplay)
    TextView txtVersion;
    @BindView(R.id.img_close)
    ImageView imgClose;
    @BindView(R.id.myprofile)
    LinearLayout myprofile;
    @BindView(R.id.myoder)
    LinearLayout myoder;
    @BindView(R.id.address)
    LinearLayout address;
    @BindView(R.id.wallet)
    LinearLayout wallets;
    @BindView(R.id.subscriptions)
    LinearLayout subscription;
    @BindView(R.id.complain)
    LinearLayout complain;
    @BindView(R.id.feedback)
    LinearLayout feedback;
    @BindView(R.id.contect)
    LinearLayout contect;
    @BindView(R.id.logout)
    LinearLayout logout;
    @BindView(R.id.about)
    LinearLayout about;
    @BindView(R.id.tramscondition)
    LinearLayout tramscondition;
    @BindView(R.id.privecy)
    LinearLayout privecy;
    @BindView(R.id.termcondition)
    LinearLayout termcondition;
    @BindView(R.id.drawer)
    LinearLayout drawer;
    @BindView(R.id.rlt_cart)
    RelativeLayout rltCart;
    @BindView(R.id.rlt_noti)
    RelativeLayout rltNoti;
    @BindView(R.id.txt_countcard)
    TextView txtCountcard;
    @BindView(R.id.img_noti)
    ImageView imgNoti;
    User user;
    public static TextView txt_countcard;
    public static HomeActivity homeActivity = null;
    public static TextView txtNoti;
    public static CustPrograssbar custPrograssbar;
    Fragment fragment1 = null;
    DatabaseHelper databaseHelper;
    SessionManager sessionManager;
    LocationManager locationManager;
    LocationListener locationListener;
    List<Address> addresses;
    private FusedLocationProviderClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);
        txtNoti = findViewById(R.id.txt_noti);
        custPrograssbar = new CustPrograssbar();
        databaseHelper = new DatabaseHelper(HomeActivity.this);
        sessionManager = new SessionManager(HomeActivity.this);
        user = sessionManager.getUserDetails("");
        homeActivity = this;
        setDrawer();
        requestPermission();
        client = LocationServices.getFusedLocationProviderClient(this);

        if (ActivityCompat.checkSelfPermission(HomeActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED ) {
            return;
        }
        client.getLastLocation().addOnSuccessListener(HomeActivity.this, new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                if (location != null) {


                    Geocoder geocoder;

                    geocoder = new Geocoder(HomeActivity.this, Locale.getDefault());

                    try {
                        addresses = geocoder.getFromLocation(location.getLatitude() , location.getLongitude(), 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                    String city = addresses.get(0).getLocality();
                    String state = addresses.get(0).getAdminArea();
                    String country = addresses.get(0).getCountryName();
                    String postalCode = addresses.get(0).getPostalCode();
                    String knownName = addresses.get(0).getFeatureName();

                    txtActiontitle.setText(address);
                    Log.d("TAG", "onLocationChanged: " + address);
                }
            }
        });
    }

    public static HomeActivity getInstance() {
        return homeActivity;
    }

    public void ShowMenu() {
        rltNoti.setVisibility(View.GONE);
        rltCart.setVisibility(View.VISIBLE);
    }

    public void setFrameMargin(int top) {
        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) lvlMainhome.getLayoutParams();
        params.setMargins(0, 0, 0, 0);
        lvlMainhome.setLayoutParams(params);
    }

    @SuppressLint("SetTextI18n")
    private void setDrawer() {
        setSupportActionBar(toolbar);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_drawer_icon);
        char first = user.getName().charAt(0);
        Log.e("first", "-->" + first);
     //   txtfirstl.setText("" + first);
      //  txtName.setText("" + user.getName());
        txtMob.setText("" + user.getMobile());
     //   txtEmail.setText("" + user.getEmail());
        titleChange();
        txt_countcard = findViewById(R.id.txt_countcard);
        Cursor res = databaseHelper.getAllData();
        if (res.getCount() == 0) {
            txt_countcard.setText("0");
        } else {
            txt_countcard.setText("" + res.getCount());
        }

        Fragment fragment = new HomeFragment();
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.fragment_frame, fragment).addToBackStack("HomePage").commit();
   //     addTextWatcher();
        if (sessionManager.getBooleanData(LOGIN)) {
            txtLogintitel.setText("Logout");
        } else {
            txtLogintitel.setText("Login");

        }
    }

 /*   public EditText passThisEditText() {
        return edSearch;
    }*/

 /*   private void addTextWatcher() {
        edSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (edSearch.getText().toString().trim().length() == 0) {
//                    fragment1 = null;
                    Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragment_frame);
                    if (fragment instanceof HomeFragment && fragment.isVisible()) {

                    } else {
                        getSupportFragmentManager().popBackStackImmediate();

                    }
                }
            }
        });
    }*/

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragment_frame);
            if (fragment instanceof HomeFragment && fragment.isVisible()) {
                finish();
            } else if (fragment instanceof OrderSumrryFragment && fragment.isVisible() && ISORDER) {
                ISORDER = false;
                Intent i = new Intent(this, HomeActivity.class);
                startActivity(i);
                finish();
                overridePendingTransition(0, 0);
            } else if (fragment instanceof MyOrderFragment && fragment.isVisible()) {
                Intent i = new Intent(this, HomeActivity.class);
                startActivity(i);
                finish();
                overridePendingTransition(0, 0);
            }
            else if (fragment instanceof SubCategoryFragment && fragment.isVisible()) {
                Intent i = new Intent(this, HomeActivity.class);
                startActivity(i);
                finish();
                overridePendingTransition(0, 0);
            }
            if (getFragmentManager().getBackStackEntryCount() > 0) {
                getFragmentManager().popBackStack();
            } else {
                super.onBackPressed();
            }
            if (fragment instanceof OrderSumrryFragment && fragment.isVisible()) {
                //edSearch.setText("");
               // lvlActionsearch.setVisibility(View.GONE);
            } else if (fragment instanceof AddressFragment && fragment.isVisible()) {
              //  edSearch.setText("");
            } else {
               // edSearch.setText("");
            }
            if (itemListFragment == null) {
                rltNoti.setVisibility(View.VISIBLE);
                rltCart.setVisibility(View.VISIBLE);
            }
        }

    }

    public void setdata() {
        rltNoti.setVisibility(View.VISIBLE);
        rltCart.setVisibility(View.VISIBLE);
    }

    public void serchviewHide() {
       // lvlActionsearch.setVisibility(View.GONE);
    }

    public void serchviewShow() {
     //   lvlActionsearch.setVisibility(View.VISIBLE);
    }

    public void titleChange(String s) {
       // txtActiontitle.setText(s);
    }

    public void titleChange() {
    //    txtActiontitle.setText("Hello " + user.getName());
    }


    public void callFragment(Fragment fragmentClass) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.fragment_frame, fragmentClass).addToBackStack("adds").commit();
        drawerLayout.closeDrawer(GravityCompat.START);
    }

    private void shareApp() {
        try {
            Intent shareIntent = new Intent(Intent.ACTION_SEND);
            shareIntent.setType("text/plain");
            shareIntent.putExtra(Intent.EXTRA_SUBJECT, getResources().getString(R.string.app_name));
            String shareMessage = "\nLet me recommend you this application\n\n";
            shareMessage = shareMessage + "https://play.google.com/store/apps/details?id=" + getApplicationContext().getPackageName() + "\n\n";
            shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
            startActivity(Intent.createChooser(shareIntent, "choose one"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @SuppressLint("SetTextI18n")
    @OnClick({R.id.img_close, R.id.myprofile, R.id.myoder, R.id.address,R.id.wallet, R.id.subscriptions, R.id.complain, R.id.feedback, R.id.contect, R.id.logout, R.id.about, R.id.privecy, /*R.id.img_search,*/ R.id.img_cart, R.id.img_noti, R.id.lvl_home, R.id.share, R.id.termcondition})
    public void onViewClicked(View view) {
        Fragment fragment;
        Bundle args;
        switch (view.getId()) {
            case R.id.img_close:
                break;
            case R.id.lvl_home:
              //  lvlActionsearch.setVisibility(View.VISIBLE);
                titleChange();
                fragment = new HomeFragment();
                callFragment(fragment);
                break;
            case R.id.myprofile:
                titleChange();

                if (sessionManager.getBooleanData(LOGIN)) {
                    startActivity(new Intent(HomeActivity.this, ProfileActivity.class));

                } else {
                    startActivity(new Intent(HomeActivity.this, LoginActivity.class));
                }
                break;
            case R.id.myoder:
                titleChange();

                if (sessionManager.getBooleanData(LOGIN)) {
                //    lvlActionsearch.setVisibility(View.GONE);
                    fragment = new MyOrderFragment();
                    callFragment(fragment);
                } else {
                    startActivity(new Intent(HomeActivity.this, LoginActivity.class));
                }
                break;
            case R.id.address:
                titleChange();

                if (sessionManager.getBooleanData(LOGIN)) {
                 //   lvlActionsearch.setVisibility(View.GONE);
                    isSelect = false;
                    fragment = new AddressFragment();
                    callFragment(fragment);
                } else {
                    startActivity(new Intent(HomeActivity.this, LoginActivity.class));
                }
                break;
            case R.id.wallet:
                titleChange();

                if (sessionManager.getBooleanData(LOGIN)) {
                    startActivity(new Intent(HomeActivity.this, MyWalletActivity.class));

                } else {
                    startActivity(new Intent(HomeActivity.this, LoginActivity.class));
                }
                break;
            case R.id.subscriptions:
                titleChange();

                if (sessionManager.getBooleanData(LOGIN)) {
                    fragment = new SubscriptionListFragment();
                    callFragment(fragment);
                } else {
                    startActivity(new Intent(HomeActivity.this, LoginActivity.class));
                }
                break;
            case R.id.complain:
                titleChange();

                if (sessionManager.getBooleanData(LOGIN)) {
                    startActivity(new Intent(HomeActivity.this, ComplainActivity.class));

                } else {
                    startActivity(new Intent(HomeActivity.this, LoginActivity.class));
                }
                break;
            case R.id.feedback:
                titleChange();

                if (sessionManager.getBooleanData(LOGIN)) {
                    startActivity(new Intent(HomeActivity.this, FeedBackActivity.class));

                } else {
                    startActivity(new Intent(HomeActivity.this, LoginActivity.class));
                }
                break;
            case R.id.contect:
                titleChange();

                startActivity(new Intent(HomeActivity.this, ContectusActivity.class));
                break;
            case R.id.logout:
                if (sessionManager.getBooleanData(LOGIN)) {
                    sessionManager.logoutUser();
                    databaseHelper.DeleteCard();
                    startActivity(new Intent(HomeActivity.this, LoginActivity.class));
                    finish();
                } else {
                    startActivity(new Intent(HomeActivity.this, LoginActivity.class));
                    finish();
                }
                break;
           /* case R.id.img_search:
                if (edSearch.getText().toString().trim().length() != 0) {
                    args = new Bundle();
                    args.putInt("id", 0);
                    args.putString("search", edSearch.getText().toString().trim());
                    fragment = new ItemListFragment();
                    fragment.setArguments(args);
                    callFragment(fragment);
                } else {
                    fragment1 = null;
                }
                break;*/
            case R.id.img_cart:
                txtActiontitle.setVisibility(View.VISIBLE);
                rltNoti.setVisibility(View.GONE);
                rltCart.setVisibility(View.VISIBLE);
               // txtActiontitle.setText("MyCart");
                fragment = new CardFragment();
                callFragment(fragment);
                Toast.makeText(getApplicationContext(),"MyCart",Toast.LENGTH_LONG).show();
                break;
            case R.id.img_noti:
                titleChange();

                startActivity(new Intent(HomeActivity.this, NotificationActivity.class));
                break;
            case R.id.share:
                shareApp();
                break;
            case R.id.about:
                titleChange();

                startActivity(new Intent(HomeActivity.this, AboutsActivity.class));
                break;
            case R.id.privecy:
                titleChange();

                startActivity(new Intent(HomeActivity.this, PrivecyPolicyActivity.class));
                break;
            case R.id.termcondition:
                titleChange();

                startActivity(new Intent(HomeActivity.this, TramsAndConditionActivity.class));
                break;
            default:
                break;
        }
        drawerLayout.closeDrawer(GravityCompat.START);
    }

    public static void notificationCount(int i) {

        if (i <= 0) {
            txtNoti.setVisibility(View.GONE);
        } else {
            txtNoti.setVisibility(View.VISIBLE);
            txtNoti.setText("" + i);
        }
    }
    private void requestPermission(){
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
    }



}
