package com.horizzon.thevegbin.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.horizzon.thevegbin.R;
import com.horizzon.thevegbin.adepter.CategoryAdp;
import com.horizzon.thevegbin.adepter.CityList_Adapter;
import com.horizzon.thevegbin.adepter.CityList_Data;
import com.horizzon.thevegbin.adepter.PincodeAdapter;
import com.horizzon.thevegbin.model.CatItem;
import com.horizzon.thevegbin.model.Category;
import com.horizzon.thevegbin.model.CityListResponse;
import com.horizzon.thevegbin.model.Payment;
import com.horizzon.thevegbin.model.PincodeList_Response;
import com.horizzon.thevegbin.model.Pincode_Data;
import com.horizzon.thevegbin.model.SubcatItem;
import com.horizzon.thevegbin.model.Times;
import com.horizzon.thevegbin.model.User;
import com.horizzon.thevegbin.model.WalletRespone_Data;
import com.horizzon.thevegbin.model.WalletTotal;
import com.horizzon.thevegbin.model.detail;
import com.horizzon.thevegbin.retrofit.APIClient;
import com.horizzon.thevegbin.retrofit.GetResult;
import com.horizzon.thevegbin.utils.CustPrograssbar;
import com.horizzon.thevegbin.utils.SessionManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyWalletActivity extends AppCompatActivity {

    @BindView(R.id.wallet_amount)
    TextView edamount;
    @BindView(R.id.btn_recharge)
    TextView btnRecharge;
    @BindView(R.id.history)
    TextView edhistory;
    User user;
    SessionManager sessionManager;
    CustPrograssbar custPrograssbar;
    String rs;
    List<detail> walletlist;
    String addamount = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_wallet);
        ButterKnife.bind(this);
        sessionManager = new SessionManager(this);
        user = sessionManager.getUserDetails("");
        custPrograssbar = new CustPrograssbar();
       // sessionManager.getWalletValue();
        sessionManager.clearWalletValue();
        rs = user.getId();

        getWalletUser(rs);

        /*Intent in= getIntent();
        Bundle b = in.getExtras();

        if(b!=null)
        {
            addamount =(String) b.get("add_amount");
        }*/

       // edamount.setText("\u20B9 " + user.getWallet());

        btnRecharge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(MyWalletActivity.this, RechargeActivity.class));

            }
        });

        edhistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MyWalletActivity.this, HistoryActivity.class));

            }
        });

    }

    private void getWalletUser(String id) {

        Call<WalletRespone_Data> call = APIClient.getInterface().getWalletTotal(id);
        call.enqueue(new Callback<WalletRespone_Data>() {
            @Override
            public void onResponse(Call<WalletRespone_Data> call, Response<WalletRespone_Data> response) {
                if (response.isSuccessful()) {


                    if (response.body().getResponseCode().equals("200")) {

                        walletlist = response.body().getDetail();
                        detail myCategory = new detail();
                        for (int i = 0; i < walletlist.size(); i++) {
                            edamount.setText("\u20B9 " + walletlist.get(i).getWalletAmount());
                        }
                    } else if (response.body().getResponseCode().equals("401")) {
                        //citylist.clear();
                       // walletlist = new ArrayList<>();
                        // getAreaList(state_id, city_id);
                        Toast.makeText(MyWalletActivity.this, "no Data Found", Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<WalletRespone_Data> call, Throwable t) {

            }
        });
    }


}