package com.horizzon.thevegbin.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.horizzon.thevegbin.R;
import com.horizzon.thevegbin.database.DatabaseHelper;
import com.horizzon.thevegbin.model.DairyItem;
import com.horizzon.thevegbin.model.Price;
import com.horizzon.thevegbin.model.Subscribe_Order;
import com.horizzon.thevegbin.model.WalletRecharge;
import com.horizzon.thevegbin.retrofit.APIClient;
import com.horizzon.thevegbin.retrofit.GetResult;
import com.horizzon.thevegbin.utils.CustPrograssbar;
import com.horizzon.thevegbin.utils.SessionManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;

public class OrderSubscribeActivity extends AppCompatActivity implements GetResult.MyListener {

    @BindView(R.id.btn_sub_start)
    TextView btnss;

    DairyItem productItem;
    ArrayList<Price> priceslist;
    String userid,pimg,pname,pprice,schDate,timeSlote,Address,qnt;
    SessionManager sessionManager;
    DatabaseHelper databaseHelper;
    CustPrograssbar custPrograssbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_subscribe);
        ButterKnife.bind(this);
        sessionManager = new SessionManager(this);
        databaseHelper = new DatabaseHelper(OrderSubscribeActivity.this);
        custPrograssbar = new CustPrograssbar();

        Intent getintent = getIntent();
        Bundle extras = getintent.getExtras();
        userid = extras.getString("user");
        pimg = extras.getString("pimg");
        pname = extras.getString("pro_name");
        pprice = extras.getString("price");
        schDate = extras.getString("sch_date");
        timeSlote = extras.getString("time_slote");
        Address = extras.getString("address");
        qnt = extras.getString("quantity");

    }

    @OnClick(R.id.btn_sub_start)
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_sub_start:
                submitsubscription();
            break;

        }

    }

    private void submitsubscription(){
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("userid", userid);
            jsonObject.put("image_path", pimg);
            jsonObject.put("product_name", pname);
            jsonObject.put("product_price", pprice);
            jsonObject.put("pick_scheduletime", schDate);
            jsonObject.put("deliverytime", timeSlote);
            jsonObject.put("address_id", Address);
            jsonObject.put("quantity", qnt);
            JsonParser jsonParser = new JsonParser();
            Call<JsonObject> call = APIClient.getInterface().getSubscibeOrder((JsonObject) jsonParser.parse(jsonObject.toString()));
            GetResult getResult = new GetResult();
            getResult.setMyListener(this);
            getResult.callForLogin(call, "1");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void callback(JsonObject result, String callNo) {

        try {
            if (callNo.equalsIgnoreCase("1")) {
                Gson gson = new Gson();
                Subscribe_Order response = gson.fromJson(result.toString(), Subscribe_Order.class);
                Toast.makeText(OrderSubscribeActivity.this, "" + response.getResponseMsg(), Toast.LENGTH_LONG).show();
                if (response.getResult().equals("true")) {

                    Intent intent = new Intent(OrderSubscribeActivity.this, HomeActivity.class);
                    startActivity(intent);
                    finish();
                }
            } else if (callNo.equalsIgnoreCase("2")) {

            }
        } catch (Exception e) {
            e.toString();
        }
    }
}