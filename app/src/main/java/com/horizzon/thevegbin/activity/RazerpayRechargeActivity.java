package com.horizzon.thevegbin.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.horizzon.thevegbin.R;
import com.horizzon.thevegbin.model.PaymentItem;
import com.horizzon.thevegbin.model.RestResponse;
import com.horizzon.thevegbin.model.UpdateAddress;
import com.horizzon.thevegbin.model.User;
import com.horizzon.thevegbin.model.WalletRecharge;
import com.horizzon.thevegbin.retrofit.APIClient;
import com.horizzon.thevegbin.retrofit.GetResult;
import com.horizzon.thevegbin.utils.SessionManager;
import com.razorpay.Checkout;
import com.razorpay.PaymentResultListener;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.ButterKnife;
import retrofit2.Call;

import static com.horizzon.thevegbin.utils.Utiles.ISVARIFICATION;
import static com.horizzon.thevegbin.utils.Utiles.isRef;

public class RazerpayRechargeActivity extends AppCompatActivity implements PaymentResultListener, GetResult.MyListener {
    SessionManager sessionManager;
    User user;
    String amount;
    String debit = "Credit";
    public static int paymentsucsses = 0;
    public static String TragectionID = "0";
    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_razerpay_recharge);
        ButterKnife.bind(this);
        sessionManager = new SessionManager(this);
        user = sessionManager.getUserDetails("");
        Intent iin= getIntent();
        Bundle b = iin.getExtras();

        if(b!=null)
        {
            amount =(String) b.get("amounts");
        }
       // paymentItem = (PaymentItem) getIntent().getSerializableExtra("detail");
        startPayment(String.valueOf(amount));
    }
    /*rzp_live_nqAzUWkicnwToi*/
    /*rzp_test_GvvJD6HTs1Koc2*/
    public void startPayment(String amount) {
        final Activity activity = this;
        final Checkout co = new Checkout();
        co.setKeyID("rzp_live_nqAzUWkicnwToi");
        try {
            JSONObject options = new JSONObject();
            options.put("name", getResources().getString(R.string.app_name));
            options.put("currency", "INR");
            double total = Double.parseDouble(amount);
            total = total * 100;
            options.put("amount", total);
            JSONObject preFill = new JSONObject();
            preFill.put("email", user.getEmail());
            preFill.put("contact", user.getMobile());
            options.put("prefill", preFill);
            co.open(activity, options);
        } catch (Exception e) {
            Toast.makeText(activity, "Error in payment: " + e.getMessage(), Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPaymentSuccess(String s) {
        sessionManager.clearPromoValue();
        sessionManager.clearPromoCode();

        TragectionID = s;
        paymentsucsses = 1;
        UserDatails(TragectionID,paymentsucsses);
        finish();
    }

    @Override
    public void onPaymentError(int i, String s) {
        finish();
    }

    private void UserDatails(String tragection,int paymentstatus) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("userid", user.getId());
            jsonObject.put("amount", amount);
            jsonObject.put("transaction_id", tragection);
            jsonObject.put("status", paymentstatus);
            jsonObject.put("debit_credit", debit);
            JsonParser jsonParser = new JsonParser();
            Call<JsonObject> call = APIClient.getInterface().getwalletRecharge((JsonObject) jsonParser.parse(jsonObject.toString()));
            GetResult getResult = new GetResult();
            getResult.setMyListener(this);
            getResult.callForLogin(call, "1");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void callback(JsonObject result, String callNo) {

        try {
            if (callNo.equalsIgnoreCase("1")) {
              //  ISVARIFICATION = -1;
                Gson gson = new Gson();
                WalletRecharge response = gson.fromJson(result.toString(), WalletRecharge.class);
                Toast.makeText(RazerpayRechargeActivity.this, "" + response.getResponseMsg(), Toast.LENGTH_LONG).show();
                if (response.getResult().equals("true")) {

                    Intent intent = new Intent(RazerpayRechargeActivity.this, MyWalletActivity.class);
                  //  intent.putExtra("add_amount", amount);
                    startActivity(intent);
                    sessionManager.clearWalletValue();
                    finish();
                }
            } else if (callNo.equalsIgnoreCase("2")) {

            }
        } catch (Exception e) {
            e.toString();
        }
    }
}