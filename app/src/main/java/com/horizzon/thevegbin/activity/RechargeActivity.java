package com.horizzon.thevegbin.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.JsonObject;
import com.horizzon.thevegbin.R;
import com.horizzon.thevegbin.model.PaymentItem;
import com.horizzon.thevegbin.model.User;
import com.horizzon.thevegbin.retrofit.GetResult;
import com.horizzon.thevegbin.utils.CustPrograssbar;
import com.horizzon.thevegbin.utils.SessionManager;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RechargeActivity extends  BaseActivity implements GetResult.MyListener{

    @BindView(R.id.ed_recharge_amount)
    EditText edRramount;
    @BindView(R.id.btn_recharge_txt)
    TextView btnRramount;
    User user;
    SessionManager sessionManager;
    CustPrograssbar custPrograssbar;
    PaymentItem paymentItem;
    private String PAYMENT;
    int nIntFromET;
    String sTextFromET;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recharge);
        ButterKnife.bind(this);
        custPrograssbar = new CustPrograssbar();

        sTextFromET = edRramount.getText().toString();

/*       btnRramount.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               Intent i = new Intent(RechargeActivity.this,RazerpayRechargeActivity.class);
               i.putExtra("amounts",sTextFromET);
               startActivity(i);
           }
       });*/
    }

    @OnClick(R.id.btn_recharge_txt)
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_recharge_txt:
                if (validation()) {
                    Intent i = new Intent(RechargeActivity.this,RazerpayRechargeActivity.class);
                    i.putExtra("amounts",edRramount.getText().toString());
                    startActivity(i);
                }
                break;

        }

    }

    @Override
    public void callback(JsonObject result, String callNo) {

    }



    private boolean validation() {
        if (edRramount.getText().toString().isEmpty()) {
            edRramount.setError("Enter Wallet Amount");
            return false;
        }
        return true;
    }
}