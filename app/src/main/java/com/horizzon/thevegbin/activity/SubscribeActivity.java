package com.horizzon.thevegbin.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Paint;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.horizzon.thevegbin.R;
import com.horizzon.thevegbin.adepter.DairyProductAdp;
import com.horizzon.thevegbin.database.DatabaseHelper;
import com.horizzon.thevegbin.database.MyCart;
import com.horizzon.thevegbin.fragment.AddressFragment;
import com.horizzon.thevegbin.fragment.CardFragment;
import com.horizzon.thevegbin.fragment.HomeFragment;
import com.horizzon.thevegbin.fragment.ItemListFragment;
import com.horizzon.thevegbin.fragment.OrderSumrryFragment;
import com.horizzon.thevegbin.model.Address;
import com.horizzon.thevegbin.model.AddressData;
import com.horizzon.thevegbin.model.DairyItem;
import com.horizzon.thevegbin.model.Payment;
import com.horizzon.thevegbin.model.Price;
import com.horizzon.thevegbin.model.ProductItem;
import com.horizzon.thevegbin.model.Subscribe_Order;
import com.horizzon.thevegbin.model.Times;
import com.horizzon.thevegbin.model.User;
import com.horizzon.thevegbin.model.WalletRecharge;
import com.horizzon.thevegbin.model.WalletRespone_Data;
import com.horizzon.thevegbin.model.detail;
import com.horizzon.thevegbin.retrofit.APIClient;
import com.horizzon.thevegbin.retrofit.GetResult;
import com.horizzon.thevegbin.utils.CustPrograssbar;
import com.horizzon.thevegbin.utils.SessionManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.horizzon.thevegbin.fragment.ItemListFragment.itemListFragment;
import static com.horizzon.thevegbin.utils.SessionManager.ADDRESS1;
import static com.horizzon.thevegbin.utils.SessionManager.CURRUNCY;
import static com.horizzon.thevegbin.utils.Utiles.isRef;
import static com.horizzon.thevegbin.utils.Utiles.isSelect;
import static com.horizzon.thevegbin.utils.Utiles.seletAddress;

public class SubscribeActivity extends AppCompatActivity implements GetResult.MyListener, View.OnClickListener {

    DairyItem productItem;
    ArrayList<Price> priceslist;
    @BindView(R.id.txtTitle_sub)
    TextView txtTitle;
    @BindView(R.id.img_icons)
    ImageView img_pro;
    @BindView(R.id.txtprice_sub)
    TextView txtprice;
    @BindView(R.id.radiogroup)
    RadioGroup rdgTime;
    @BindView(R.id.txt_address)
    TextView txtAddress;
    @BindView(R.id.txt_changeadress)
    TextView txtChangeAds;
    @BindView(R.id.walletAmunts)
    TextView txtWalletamount;

    @BindView(R.id.spinner)
    Spinner spinner;
    @BindView(R.id.lvl_pricelist)
    LinearLayout lvlPricelist;
    @BindView(R.id.lvlbacket)
    LinearLayout lvlbacket;
    @BindView(R.id.txt_dairyprice)
    TextView txtPricetotl;
    @BindView(R.id.txt_dairyitem)
    TextView txtCountPr;

    @BindView(R.id.first)
    Button first_cal;
    @BindView(R.id.second)
    Button second_cal;
    @BindView(R.id.third)
    Button third_cal;
    @BindView(R.id.txt_sub_start_Date)
    EditText txtdate;

    List<DairyItem> dairyDataList;
    DairyProductAdp itemAdp;
    Address Selectaddress;
    RadioButton selectTime;
    List<detail> walletlist;

    TextView txtcount;
    Unbinder unbinder;
    SessionManager sessionManager;
    DatabaseHelper databaseHelper;
    CustPrograssbar custPrograssbar;
    User user;
    String rs;
    public static SubscribeActivity subscribactivity;
    public static String daily = "Daily";
    public static String alt = "Alternate";
    public static String every = "Every 3 Day";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subscribe);
        ButterKnife.bind(this);
        sessionManager = new SessionManager(this);
        user = sessionManager.getUserDetails("");
        databaseHelper = new DatabaseHelper(SubscribeActivity.this);
        custPrograssbar = new CustPrograssbar();
        getTimeSlot();
        rs = user.getId();
        getUserWallet(rs);

        Intent getintent = getIntent();
        productItem = getintent.getParcelableExtra("MyClass");
        priceslist = getIntent().getParcelableArrayListExtra("MyList");

        //  productItem = getintent.getParcelableExtra("pro_items");
        //  priceslist = getIntent().getParcelableArrayListExtra("Price_prodlist");
        if (productItem != null) {
            txtTitle.setText("" + productItem.getProductName());
            Glide.with(SubscribeActivity.this).load(APIClient.Base_URL + "/" + productItem.getProductImage()).placeholder(R.drawable.empty).into(img_pro);
            List<String> Arealist = new ArrayList<>();
            for (int i = 0; i < priceslist.size(); i++) {
                Arealist.add(priceslist.get(i).getProductType());
            }
            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, R.layout.spinner_layout, Arealist);
            spinner.setAdapter(dataAdapter);
        }

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (productItem.getDiscount() > 0) {
                    double res = 0;
                    res = (Double.parseDouble(priceslist.get(position).getProductPrice()) * productItem.getDiscount()) / 100;
                    res = Double.parseDouble(priceslist.get(position).getProductPrice()) -  res;

                    txtprice.setText(new DecimalFormat("##.##").format(res));
                } else {
                    txtprice.setText(priceslist.get(position).getProductPrice());
                }
                setJoinPlayrList(lvlPricelist, productItem, priceslist.get(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }


    @OnClick({R.id.first, R.id.second, R.id.third, R.id.txt_changeadress, R.id.txt_gocart})
    public void onViewClicked(View view) {
        Fragment fragment;
        switch (view.getId()) {
            case R.id.first:
                openDatePickerDialog(daily);
                break;
            case R.id.second:
                setDateField_s(alt);
                break;
            case R.id.third:
                setDateField_t(every);
                break;
            case R.id.txt_changeadress:
                isSelect = true;
                //     Intent i = new Intent(SubscribeActivity.this,AddressListActivity.class);
                //     startActivity(i);
                startActivity(new Intent(SubscribeActivity.this, AddressListActivity.class).putExtra("MyClass", productItem).putParcelableArrayListExtra("MyList", priceslist));
                break;
            case R.id.txt_gocart:
                if (validation()) {
                    startActivity(new Intent(SubscribeActivity.this, OrderSubscribeActivity.class)
                            .putExtra("user", user.getId())
                            .putExtra("pimg", productItem.getProductImage())
                            .putExtra("pro_name", txtTitle.getText().toString())
                            .putExtra("price", txtprice.getText().toString())
                            .putExtra("sch_date", txtdate.getText().toString())
                            .putExtra("time_slote", selectTime.getText().toString())
                            .putExtra("address", txtAddress.getText().toString())
                            .putExtra("quantity", txtcount.getText().toString()));
                }
                // submitsubscription();
                break;
            default:
        }
    }

    @Override
    public void callback(JsonObject result, String callNo) {

        try {
            if (callNo.equalsIgnoreCase("1")) {
                RadioButton rdbtn = null;
                Log.e("Response", "->" + result);
                Gson gson = new Gson();
                Times times = gson.fromJson(result.toString(), Times.class);
                for (int i = 0; i < times.getData().size(); i++) {
                    rdbtn = new RadioButton(getApplicationContext());
                    rdbtn.setId(View.generateViewId());
                    rdbtn.setText(times.getData().get(i).getMintime() + " - " + times.getData().get(i).getMaxtime());
                    rdbtn.setOnClickListener(this);
                    rdgTime.addView(rdbtn);
                }
                rdgTime.check(rdbtn.getId());

            } else if (callNo.equalsIgnoreCase("2")) {
                Gson gson = new Gson();

                AddressData addressData = gson.fromJson(result.toString(), AddressData.class);
                if (addressData.getResult().equalsIgnoreCase("true")) {
                    if (addressData.getResultData().size() != 0) {
                        Selectaddress = addressData.getResultData().get(seletAddress);
                        if (Selectaddress.isIS_UPDATE_NEED()) {
                            Toast.makeText(getApplicationContext(), "Please Update Your Area Name.Because It's Not match with Our Delivery Location", Toast.LENGTH_LONG).show();
                            startActivity(new Intent(getApplicationContext(), AddressActivity.class).putExtra("MyClass", Selectaddress));
                        } else {
                            txtAddress.setText(Selectaddress.getHno() + "," + Selectaddress.getSociety() + "," + Selectaddress.getArea() + "," + Selectaddress.getLandmark() + "," + Selectaddress.getName());
                        }


                    } else {
                        Toast.makeText(getApplicationContext(), "Please add your address ", Toast.LENGTH_LONG).show();

                        AddressFragment fragment = new AddressFragment();
                        HomeActivity.getInstance().callFragment(fragment);
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Please add your address ", Toast.LENGTH_LONG).show();

                    AddressFragment fragment = new AddressFragment();
                    HomeActivity.getInstance().callFragment(fragment);
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void openDatePickerDialog(String daily) {
        final Calendar c = Calendar.getInstance();

        int y = c.get(Calendar.YEAR);
        int m = c.get(Calendar.MONTH);
        int d = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog dp = new DatePickerDialog(SubscribeActivity.this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        String erg = "";
                        erg = String.valueOf(dayOfMonth);
                        erg += "." + String.valueOf(monthOfYear + 1);
                        erg += "." + year;

                        SimpleDateFormat simpledateformat = new SimpleDateFormat("EEEE");
                        Date date = new Date(year, monthOfYear, dayOfMonth - 1);
                        String dayOfWeek = simpledateformat.format(date);

                        txtdate.setText(dayOfWeek + "," + erg + " " + daily);

                    }

                }, y, m, d);
        dp.show();
    }


    private void setDateField_s(String Atlernate) {
        final Calendar c = Calendar.getInstance();

        int y = c.get(Calendar.YEAR);
        int m = c.get(Calendar.MONTH);
        int d = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog dp = new DatePickerDialog(SubscribeActivity.this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        String erg = "";
                        erg = String.valueOf(dayOfMonth);
                        erg += "." + String.valueOf(monthOfYear + 1);
                        erg += "." + year;

                        SimpleDateFormat simpledateformat = new SimpleDateFormat("EEEE");
                        Date date = new Date(year, monthOfYear, dayOfMonth - 1);
                        String dayOfWeek = simpledateformat.format(date);

                        txtdate.setText(dayOfWeek + "," + erg + " " + Atlernate);

                    }

                }, y, m, d);
        dp.show();

    }

    private void setDateField_t(String every) {
        final Calendar c = Calendar.getInstance();

        int y = c.get(Calendar.YEAR);
        int m = c.get(Calendar.MONTH);
        int d = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog dp = new DatePickerDialog(SubscribeActivity.this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        String erg = "";
                        erg = String.valueOf(dayOfMonth);
                        erg += "." + String.valueOf(monthOfYear + 1);
                        erg += "." + year;

                        SimpleDateFormat simpledateformat = new SimpleDateFormat("EEEE");
                        Date date = new Date(year, monthOfYear, dayOfMonth - 1);
                        String dayOfWeek = simpledateformat.format(date);

                        txtdate.setText(dayOfWeek + "," + erg + " " + every);

                    }

                }, y, m, d);
        dp.show();

    }

    private void setJoinPlayrList(LinearLayout lnrView, DairyItem datum, Price price) {
        lnrView.removeAllViews();
        final int[] count = {0};
        DatabaseHelper helper = new DatabaseHelper(lnrView.getContext());
        LayoutInflater inflater = LayoutInflater.from(getApplicationContext());
        View view = inflater.inflate(R.layout.custome_prize, null);
        txtcount = view.findViewById(R.id.txtcount);
        LinearLayout lvl_addremove = view.findViewById(R.id.lvl_addremove);
        LinearLayout lvl_addcart = view.findViewById(R.id.lvl_addcart);
        LinearLayout img_mins = view.findViewById(R.id.img_mins);
        LinearLayout img_plus = view.findViewById(R.id.img_plus);
        MyCart myCart = new MyCart();
        myCart.setPID(datum.getId());
        myCart.setImage(datum.getProductImage());
        myCart.setTitle(datum.getProductName());
        myCart.setWeight(price.getProductType());
        myCart.setCost(price.getProductPrice());
        myCart.setDiscount(datum.getDiscount());
        myCart.setStock_no(datum.getNoOfStock());
        int qrt = helper.getCard(myCart.getPID(), myCart.getCost());
        if (qrt != -1) {
            count[0] = qrt;
            txtcount.setText("" + count[0]);
            lvl_addremove.setVisibility(View.VISIBLE);
            lvl_addcart.setVisibility(View.GONE);
        } else {
            lvl_addremove.setVisibility(View.GONE);
            lvl_addcart.setVisibility(View.VISIBLE);

        }
        img_mins.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                count[0] = Integer.parseInt(txtcount.getText().toString());

                count[0] = count[0] - 1;
                if (count[0] <= 0) {
                    txtcount.setText("" + count[0]);
                    lvl_addremove.setVisibility(View.GONE);
                    lvl_addcart.setVisibility(View.VISIBLE);
                    helper.deleteRData(myCart.getPID(), myCart.getCost());
                } else {
                    txtcount.setVisibility(View.VISIBLE);
                    txtcount.setText("" + count[0]);
                    myCart.setQty(String.valueOf(count[0]));
                    helper.insertData(myCart);
                }
                updateItem();
            }
        });

        img_plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                count[0] = Integer.parseInt(txtcount.getText().toString());
                count[0] = count[0] + 1;
                txtcount.setText("" + count[0]);
                myCart.setQty(String.valueOf(count[0]));
                Log.e("INsert", "--> " + helper.insertData(myCart));
                updateItem();
            }
        });
        lvl_addcart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lvl_addcart.setVisibility(View.GONE);
                lvl_addremove.setVisibility(View.VISIBLE);
                count[0] = Integer.parseInt(txtcount.getText().toString());
                count[0] = count[0] + 1;
                txtcount.setText("" + count[0]);
                myCart.setQty(String.valueOf(count[0]));
                Log.e("INsert", "--> " + helper.insertData(myCart));
                updateItem();
            }
        });
        lnrView.addView(view);

    }

    public void updateItem() {
        try {
            Cursor res = databaseHelper.getAllData();
            if (res.getCount() == 0) {
                lvlbacket.setVisibility(View.GONE);
            } else {
                lvlbacket.setVisibility(View.VISIBLE);

                double totalRs = 0;
                double ress = 0;
                int totalItem = 0;
                while (res.moveToNext()) {
                    MyCart rModel = new MyCart();
                    rModel.setCost(res.getString(5));
                    rModel.setQty(res.getString(6));
                    rModel.setDiscount(res.getInt(7));
                    ress = (Double.parseDouble(res.getString(5)) * rModel.getDiscount()) / 100;
                    ress = Double.parseDouble(res.getString(5)) - ress;
                    double temp = Double.parseDouble(res.getString(6)) * ress;
                    totalItem = totalItem + Integer.parseInt(res.getString(6));
                    totalRs = totalRs + temp;
                }

                txtCountPr.setText(totalItem + " Items");
                txtPricetotl.setText(sessionManager.getStringData(CURRUNCY) + new DecimalFormat("##.##").format(totalRs));
            }
        } catch (Exception e) {
            e.printStackTrace();

        }
    }
    /*Fragment fragment = null;
    @OnClick(R.id.txt_changeadress)
    public void onViewClicked() {
        fragment = new AddressFragment();
    }*/

    private void getTimeSlot() {
        custPrograssbar.PrograssCreate(getApplicationContext());
        JSONObject jsonObject = new JSONObject();
        JsonParser jsonParser = new JsonParser();
        Call<JsonObject> call = APIClient.getInterface().getTimeslot((JsonObject) jsonParser.parse(jsonObject.toString()));
        GetResult getResult = new GetResult();
        getResult.setMyListener(this);
        getResult.callForLogin(call, "1");
    }

    @Override
    public void onClick(View v) {

        try {

            int selectedId = rdgTime.getCheckedRadioButtonId();
            selectTime = rdgTime.findViewById(selectedId);
            Log.d("TAG", "onClicktime: " + selectTime);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getUserWallet(String id) {

        Call<WalletRespone_Data> call = APIClient.getInterface().getWalletTotal(id);
        call.enqueue(new Callback<WalletRespone_Data>() {
            @Override
            public void onResponse(Call<WalletRespone_Data> call, Response<WalletRespone_Data> response) {
                if (response.isSuccessful()) {


                    if (response.body().getResponseCode().equals("200")) {

                        walletlist = response.body().getDetail();
                        detail myCategory = new detail();
                        for (int i = 0; i < walletlist.size(); i++) {
                            txtWalletamount.setText("\u20B9 " + walletlist.get(i).getWalletAmount());
                        }
                    } else if (response.body().getResponseCode().equals("401")) {
                        //citylist.clear();
                        // walletlist = new ArrayList<>();
                        // getAreaList(state_id, city_id);
                        Toast.makeText(SubscribeActivity.this, "no Data Found", Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<WalletRespone_Data> call, Throwable t) {

            }
        });
    }

    public boolean validation() {
        if (txtdate.getText().toString().isEmpty()) {
            txtdate.setError("Select date");
            return false;
        }
        return true;
    }
    @Override
    public void onResume() {
        //will be executed onResume
        super.onResume();
        try {
            if (sessionManager != null) {
                Selectaddress = sessionManager.getAddress(ADDRESS1);
                if (Selectaddress != null) {
                    txtAddress.setText(Selectaddress.getHno() + "," + Selectaddress.getSociety() + "," + Selectaddress.getArea() + "," + Selectaddress.getLandmark() + "," + Selectaddress.getName());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}