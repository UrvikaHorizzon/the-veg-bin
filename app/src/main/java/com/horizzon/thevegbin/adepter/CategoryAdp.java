package com.horizzon.thevegbin.adepter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.horizzon.thevegbin.R;
import com.horizzon.thevegbin.activity.HomeActivity;
import com.horizzon.thevegbin.fragment.DairyListFragment;
import com.horizzon.thevegbin.fragment.ItemListFragment;
import com.horizzon.thevegbin.model.CatItem;
import com.horizzon.thevegbin.retrofit.APIClient;

import java.util.List;

public class CategoryAdp extends RecyclerView.Adapter<CategoryAdp.MyViewHolder> {
    private Context mContext;
    private List<CatItem> categoryList;
    private RecyclerTouchListener listener;

    public interface RecyclerTouchListener {
        public void onClickItem(String titel, int position);

        public void onLongClickItem(View v, int position);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title;
        public ImageView thumbnail, overflow;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.txt_title);
            thumbnail = view.findViewById(R.id.imageView);
        }

        public void setEventNameName(String TheEventName) {
            title.setText(TheEventName);
           /* if (categoryList.size() != 0) {
                removeItem(categoryList.size() - 1);
                categoryList.add(title);
                categoryList.add("Add");
            } else
                categoryList.add("Add");*/
        }
    }

    public CategoryAdp(Context mContext, List<CatItem> categoryList, final RecyclerTouchListener listener) {
        this.mContext = mContext;
        this.categoryList = categoryList;
        this.listener = listener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_category, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        CatItem category = categoryList.get(position);
        holder.setEventNameName(position + "");
        if (position == 0) {
            //  holder.thumbnail.setImageResource(R.drawable.wallet);
            //Glide.with(mContext).load(APIClient.Base_URL + "/" + category.getCatimg()).thumbnail(Glide.with(mContext).load(holder.thumbnail));
            // holder.title.setText("Add");
        }

        holder.title.setText(category.getCatname());
        Glide.with(mContext).load(APIClient.Base_URL + "/" + category.getCatimg()).thumbnail(Glide.with(mContext).load(R.drawable.dairy)).into(holder.thumbnail);
        holder.thumbnail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (position == 0) {
                    Fragment fragment = new DairyListFragment();
                    HomeActivity.getInstance().callFragment(fragment);
                } else {
                    listener.onClickItem(category.getCatname(), Integer.parseInt(category.getId()));
                }
            }
        });
    }


    @Override
    public int getItemCount() {
        return categoryList.size();
    }
}