package com.horizzon.thevegbin.adepter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Paint;
import android.graphics.Point;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.horizzon.thevegbin.MainActivity;
import com.horizzon.thevegbin.R;
import com.horizzon.thevegbin.activity.CustomDialog;
import com.horizzon.thevegbin.activity.HomeActivity;
import com.horizzon.thevegbin.activity.ItemDetailsActivity;
import com.horizzon.thevegbin.activity.SubscribeActivity;
import com.horizzon.thevegbin.database.DatabaseHelper;
import com.horizzon.thevegbin.database.MyCart;
import com.horizzon.thevegbin.fragment.DairyListFragment;
import com.horizzon.thevegbin.model.DairyItem;
import com.horizzon.thevegbin.model.Price;
import com.horizzon.thevegbin.retrofit.APIClient;
import com.horizzon.thevegbin.utils.SessionManager;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.horizzon.thevegbin.fragment.DairyListFragment.dairyListFragment;
import static com.horizzon.thevegbin.utils.SessionManager.CURRUNCY;

public class DairyProductAdp extends RecyclerView.Adapter<DairyProductAdp.ViewHolder> {

    private List<DairyItem> mData;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;
    Context mContext;
    SessionManager sessionManager;

    public DairyProductAdp(Context context, List<DairyItem> data) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
        this.mContext = context;
        WindowManager wm = (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        sessionManager = new SessionManager(mContext);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.item_dairyprodict, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(DairyProductAdp.ViewHolder holder, int position) {
        DairyItem datum = mData.get(position);
        Glide.with(mContext).load(APIClient.Base_URL + "/" + datum.getProductImage()).thumbnail(Glide.with(mContext).load(R.drawable.ezgifresize)).into(holder.imgIcon);
        holder.txtTitle.setText("" + datum.getProductName());
        holder.imgIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                View view;    view = LayoutInflater.from(mContext).inflate(R.layout.custom_dialog, null);
                TextView title = (TextView) view.findViewById(R.id.ivtitle);
                TextView desc = (TextView) view.findViewById(R.id.tvContent);
                ImageView imageButton = (ImageView) view.findViewById(R.id.ivOffer);
                title.setText(datum.getProductName());
                desc.setText(datum.getShortDesc());

                builder.setNegativeButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                    }
                });

                Glide.with(mContext).load(APIClient.Base_URL + "/" + datum.getProductImage()).thumbnail(Glide.with(mContext).load(R.drawable.ezgifresize)).into(imageButton);
                builder.setView(view);
                builder.show();
            }
        });
        if (datum.getDiscount() > 0) {
            holder.lvlOffer.setVisibility(View.VISIBLE);
            holder.txtOffer.setText(datum.getDiscount() + "% Off");
        } else {
            holder.lvlOffer.setVisibility(View.GONE);
        }
        List<String> Arealist = new ArrayList<>();
        for (int i = 0; i < datum.getPrice().size(); i++) {

            Arealist.add(datum.getPrice().get(i).getProductType());
        }
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(mContext, R.layout.spinner_layout, Arealist);
        dataAdapter.setDropDownViewResource(R.layout.spinner_layout);
        holder.spinner.setAdapter(dataAdapter);
        holder.spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (datum.getDiscount() > 0) {
                    double res = (Double.parseDouble(datum.getPrice().get(position).getProductPrice()) / 100.0f) * datum.getDiscount();
                    res = Double.parseDouble(datum.getPrice().get(position).getProductPrice()) - res;
                    holder.txtItemOffer.setText(sessionManager.getStringData(CURRUNCY) + datum.getPrice().get(position).getProductPrice());
                    holder.txtItemOffer.setPaintFlags(holder.txtItemOffer.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                    holder.txtPrice.setText(sessionManager.getStringData(CURRUNCY) + new DecimalFormat("##.##").format(res));
                    holder.txtItemOffer.setText(sessionManager.getStringData(CURRUNCY) + datum.getPrice().get(position).getProductPrice());
                } else {
                    holder.txtItemOffer.setVisibility(View.GONE);
                    holder.txtPrice.setText(sessionManager.getStringData(CURRUNCY) + datum.getPrice().get(position).getProductPrice());
                }
                setJoinPlayrList(holder.lvlSubitem, datum, datum.getPrice().get(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

       /* holder.btn_subcribe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mContext.startActivity(new Intent(mContext, SubscribeActivity.class).putExtra("MyClass", datum).putParcelableArrayListExtra("MyList", datum.getPrice()));
                *//*Fragment fragments = new SubscribeFragment();
                HomeActivity.getInstance().callFragment(fragments);*//*

            }
        });*/
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.txtTitle)
        TextView txtTitle;
        @BindView(R.id.txt_offer)
        TextView txtOffer;
        @BindView(R.id.txt_item_offer)
        TextView txtItemOffer;
        @BindView(R.id.txt_price)
        TextView txtPrice;
        @BindView(R.id.lvl_subitem)
        LinearLayout lvlSubitem;
        @BindView(R.id.lvl_offer)
        LinearLayout lvlOffer;
        @BindView(R.id.spinner)
        Spinner spinner;
        @BindView(R.id.img_icon)
        ImageView imgIcon;
      //  @BindView(R.id.subscribe_btn)
      //  Button btn_subcribe;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }
    }

    void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }

    private void setJoinPlayrList(LinearLayout lnrView, DairyItem datum, Price price) {
        lnrView.removeAllViews();
        final int[] count = {0};
        DatabaseHelper helper = new DatabaseHelper(lnrView.getContext());
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.custome_subscribe_btn, null);
        TextView txtcount = view.findViewById(R.id.txtcount);
        LinearLayout lvl_addremove = view.findViewById(R.id.lvl_addremove);
        LinearLayout lvl_addcart = view.findViewById(R.id.lvl_addcart);
        LinearLayout img_mins = view.findViewById(R.id.img_mins);
        LinearLayout img_plus = view.findViewById(R.id.img_plus);

        txtcount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mContext.startActivity(new Intent(mContext, SubscribeActivity.class).putExtra("MyClass", datum).putParcelableArrayListExtra("MyList", datum.getPrice()));
            }
        });

        lnrView.addView(view);
    }

    public void filterList(ArrayList<DairyItem> filteredList) {
        mData = filteredList;
        notifyDataSetChanged();
    }
}