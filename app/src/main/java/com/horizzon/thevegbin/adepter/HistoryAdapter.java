package com.horizzon.thevegbin.adepter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.horizzon.thevegbin.R;
import com.horizzon.thevegbin.model.Response_Data;
import com.horizzon.thevegbin.model.SubcatItem;
import com.horizzon.thevegbin.retrofit.APIClient;

import java.util.List;

public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.MyViewHolder> {

    private Context mContext;
    private List<Response_Data> categoryList;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView amount,date,debitcreadit;
        public MyViewHolder(View view) {
            super(view);
            amount = (TextView) view.findViewById(R.id.txtamount);
            date = (TextView) view.findViewById(R.id.txtDate);
            debitcreadit = (TextView) view.findViewById(R.id.txtdebitcredit);
        }


    }
    public HistoryAdapter(List<Response_Data> categoryList) {
        this.mContext = mContext;
        this.categoryList = categoryList;

    }
    @Override
    public HistoryAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_history, parent, false);


        return new HistoryAdapter.MyViewHolder(itemView);
    }
    @Override
    public void onBindViewHolder(final HistoryAdapter.MyViewHolder holder,final int position) {
        categoryList.get(position);

        Response_Data sched = categoryList.get(position);
        holder.amount.setText("\u20B9 " + sched.getAmount());
        holder.date.setText(sched.getCreatedAt());
        holder.debitcreadit.setText(sched.getDebitCredit());

        if(holder.debitcreadit.getText().toString().equals("Credit"))
        {
            holder.debitcreadit.setTextColor(Color.parseColor("#008000"));
        }
        else
        {
            holder.debitcreadit.setTextColor(Color.RED);
        }


    }
    @Override
    public int getItemCount() {
        return categoryList.size();
    }

}