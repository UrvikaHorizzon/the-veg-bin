package com.horizzon.thevegbin.adepter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.horizzon.thevegbin.R;
import com.horizzon.thevegbin.activity.HomeActivity;
import com.horizzon.thevegbin.model.Subscribe_DataList;
import com.horizzon.thevegbin.model.Subscription_StatusData;
import com.horizzon.thevegbin.retrofit.APIClient;
import com.horizzon.thevegbin.retrofit.GetResult;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import retrofit2.Call;

import static com.horizzon.thevegbin.retrofit.GetResult.myListener;

public class SubscribeListAdapter extends RecyclerView.Adapter<SubscribeListAdapter.MyViewHolder> implements GetResult.MyListener {

    private Context mContext;
    private List<Subscribe_DataList> categoryList;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView proName, proQnt, proMRP;
        ImageView img_product;
        Button proStatus;

        public MyViewHolder(View view) {
            super(view);
            img_product = (ImageView) view.findViewById(R.id.img_icons_list);
            proName = (TextView) view.findViewById(R.id.txtTitle_list);
            proQnt = (TextView) view.findViewById(R.id.txtqnt_list);
            proMRP = (TextView) view.findViewById(R.id.txtprice_list);
            proStatus = (Button) view.findViewById(R.id.txtstatus_list);
        }


    }

    public SubscribeListAdapter(Context mContext, List<Subscribe_DataList> categoryList) {
        this.mContext = mContext;
        this.categoryList = categoryList;

    }

    @Override
    public SubscribeListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_subscribelist, parent, false);


        return new SubscribeListAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final SubscribeListAdapter.MyViewHolder holder, final int position) {
        categoryList.get(position);
        Subscribe_DataList sched = categoryList.get(position);

        Glide.with(mContext).load(APIClient.Base_URL + "/" + sched.getImagePath()).thumbnail(Glide.with(mContext).load(R.drawable.ezgifresize)).into(holder.img_product);
        holder.proName.setText(sched.getProductName());
        holder.proQnt.setText(sched.getPickScheduletime());
        holder.proMRP.setText("MRP" + "\u20B9 " + sched.getProductPrice());
        holder.proStatus.setText("status" + sched.getStatus());

        if (holder.proStatus.getText().toString().equals("1")) {
            holder.proStatus.setText("Active");
            holder.proStatus.setTextColor(mContext.getResources().getColor(R.color.colorPrimary));
        } else {
            holder.proStatus.setText("Deactive");
            holder.proStatus.setTextColor(mContext.getResources().getColor(R.color.colorPrimary));
        }

        holder.proStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Build an AlertDialog
                AlertDialog.Builder dialog = new AlertDialog.Builder(mContext);
                dialog.setMessage("Your Subscription is DeActivate successfully");
                dialog.setTitle("Subscription");
                dialog.setIcon(R.drawable.history_logo);
                dialog.setPositiveButton("YES",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                               // getDeletSub();
                                JSONObject jsonObject = new JSONObject();
                                try {
                                    jsonObject.put("id", sched.getId());
                                    jsonObject.put("status", "0");
                                    JsonParser jsonParser = new JsonParser();
                                    Call<JsonObject> call = APIClient.getInterface().getSubscribStatus((JsonObject) jsonParser.parse(jsonObject.toString()));
                                    GetResult getResult = new GetResult();
                                    getResult.setMyListener(myListener);
                                    getResult.callForLogin(call, "1");
                                    Intent intent = new Intent(mContext, HomeActivity.class);
                                    mContext.startActivity(intent);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                Toast.makeText(mContext, "Subscription is DeActivate successfully", Toast.LENGTH_LONG).show();
                            }
                        });
                dialog.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                       // Toast.makeText(mContext, "cancel is clicked", Toast.LENGTH_LONG).show();
                    }
                });
                AlertDialog alertDialog = dialog.create();
                alertDialog.show();
            }
        });


    }

    @Override
    public int getItemCount() {
        return categoryList.size();
    }

    @Override
    public void callback(JsonObject result, String callNo) {

        try {
            if (callNo.equalsIgnoreCase("1")) {
                Gson gson = new Gson();
                Subscription_StatusData response = gson.fromJson(result.toString(), Subscription_StatusData.class);
                //  Toast.makeText(mContext, "" + response.getResponseMsg(), Toast.LENGTH_LONG).show();
                if (response.getResult().equals("true")) {

                    Intent intent = new Intent(mContext, HomeActivity.class);
                    mContext.startActivity(intent);
                }
            } else if (callNo.equalsIgnoreCase("2")) {

            }
        } catch (Exception e) {
            e.toString();
        }
    }

}