package com.horizzon.thevegbin.fragment;

import android.database.Cursor;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.horizzon.thevegbin.R;
import com.horizzon.thevegbin.activity.HomeActivity;
import com.horizzon.thevegbin.adepter.DairyProductAdp;
import com.horizzon.thevegbin.database.DatabaseHelper;
import com.horizzon.thevegbin.database.MyCart;
import com.horizzon.thevegbin.model.DairyItem;
import com.horizzon.thevegbin.model.ProductItem;
import com.horizzon.thevegbin.model.Subscribe;
import com.horizzon.thevegbin.retrofit.APIClient;
import com.horizzon.thevegbin.retrofit.GetResult;
import com.horizzon.thevegbin.utils.SessionManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Call;

import static com.horizzon.thevegbin.utils.SessionManager.CURRUNCY;
import static com.horizzon.thevegbin.utils.SessionManager.ISCART;

public class DairyListFragment extends Fragment implements GetResult.MyListener {

    @BindView(R.id.lvlbacket)
    LinearLayout lvlbacket;
    @BindView(R.id.txt_dairyitem)
    TextView txtItem;
    @BindView(R.id.txt_notfound)
    TextView txtNotfound;
    @BindView(R.id.txt_dairyprice)
    TextView txtPrice;
    @BindView(R.id.lvl_notfound)
    LinearLayout lvlNotfound;
    @BindView(R.id.dairylist_recycler)
    RecyclerView rvdairylist;
    @BindView(R.id.ed_search_product)
    EditText edserch;

    List<DairyItem> dairyDataList;
    DairyProductAdp itemAdp;

    Unbinder unbinder;
    SessionManager sessionManager;
    DatabaseHelper databaseHelper;
    private StaggeredGridLayoutManager gaggeredGridLayoutManager;

    public static DairyListFragment dairyListFragment;

    public DairyListFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_dairy_list, container, false);
        HomeActivity.getInstance().setFrameMargin(60);
        unbinder = ButterKnife.bind(this, view);
        databaseHelper = new DatabaseHelper(getActivity());
        dairyListFragment = this;
        sessionManager = new SessionManager(getActivity());
        addTextWatcher();
        getDairyProduct();

        rvdairylist.setHasFixedSize(true);
        dairyDataList = new ArrayList<>();
        gaggeredGridLayoutManager = new StaggeredGridLayoutManager(1, 1);
        rvdairylist.setLayoutManager(gaggeredGridLayoutManager);
        itemAdp = new DairyProductAdp(getActivity(), dairyDataList);
        rvdairylist.setAdapter(itemAdp);

        Cursor res = databaseHelper.getAllData();
        if (res.getCount() == 0) {
            lvlbacket.setVisibility(View.GONE);
        } else {
            lvlbacket.setVisibility(View.VISIBLE);
            updateItem();
        }

        return view;
    }

    public EditText passThisEditText() {
        return edserch;
    }

    private void addTextWatcher() {
        edserch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                filter(s.toString());
            }
        });
    }

    private void filter(String text) {
        ArrayList<DairyItem> filteredList = new ArrayList<>();
        for (DairyItem item : dairyDataList) {
            if (item.getProductName().toLowerCase().contains(text.toLowerCase())) {
                filteredList.add(item);
            }
        }
        itemAdp.filterList(filteredList);
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    private void getDairyProduct() {
        JSONObject jsonObject = new JSONObject();

        JsonParser jsonParser = new JsonParser();
        Call<JsonObject> call = APIClient.getInterface().getDairyProduct((JsonObject) jsonParser.parse(jsonObject.toString()));
        GetResult getResult = new GetResult();
        getResult.setMyListener(this);
        getResult.callForLogin(call, "1");
        HomeActivity.custPrograssbar.PrograssCreate(getActivity());
    }


    @Override
    public void callback(JsonObject result, String callNo) {

        HomeActivity.custPrograssbar.ClosePrograssBar();
        JSONObject jsonObject;
        try {
            Gson gson = new Gson();
            jsonObject = new JSONObject(result.toString());
            Subscribe product = gson.fromJson(jsonObject.toString(), Subscribe.class);
            if (product.getResult().equals("true")) {
                if (product.getData().get(0).getSubscribe().equalsIgnoreCase("Yes")) {
                    dairyDataList.clear();
                    dairyDataList.addAll(product.getData());
                    lvlNotfound.setVisibility(View.GONE);
                } else {
                    Toast.makeText(getActivity(), "Product List Not Found!", Toast.LENGTH_SHORT).show();

                }
                dairyDataList.clear();
                dairyDataList.addAll(product.getData());
                lvlNotfound.setVisibility(View.GONE);

            } else {
                lvlbacket.setVisibility(View.GONE);
                lvlNotfound.setVisibility(View.VISIBLE);
                txtNotfound.setText("" + product.getResponseMsg());
            }
            itemAdp.notifyDataSetChanged();
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    public void updateItem() {
        try {
            Cursor res = databaseHelper.getAllData();
            if (res.getCount() == 0) {
                lvlbacket.setVisibility(View.GONE);
            } else {
                lvlbacket.setVisibility(View.VISIBLE);

                double totalRs = 0;
                double ress = 0;
                int totalItem = 0;
                while (res.moveToNext()) {
                    MyCart rModel = new MyCart();
                    rModel.setCost(res.getString(5));
                    rModel.setQty(res.getString(6));
                    rModel.setDiscount(res.getInt(7));
                    ress = (Double.parseDouble(res.getString(5)) * rModel.getDiscount()) / 100;
                    ress = Double.parseDouble(res.getString(5)) - ress;
                    double temp = Double.parseDouble(res.getString(6)) * ress;
                    totalItem = totalItem + Integer.parseInt(res.getString(6));
                    totalRs = totalRs + temp;
                }

                txtItem.setText(totalItem + " Items");
                txtPrice.setText(sessionManager.getStringData(CURRUNCY) + new DecimalFormat("##.##").format(totalRs));
            }
        } catch (Exception e) {
            e.printStackTrace();

        }
    }


    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.img_cart);
        if (item != null)
            item.setVisible(false);
    }

    @Override
    public void onResume() {
        super.onResume();
        HomeActivity.getInstance().serchviewShow();
        HomeActivity.getInstance().setFrameMargin(60);
        if (ISCART) {
            ISCART = false;
            CardFragment fragment = new CardFragment();
            HomeActivity.getInstance().callFragment(fragment);
        } else if (itemAdp != null) {
            itemAdp.notifyDataSetChanged();
        }
    }

    @OnClick(R.id.txt_gocart)
    public void onViewClicked() {
        CardFragment fragment = new CardFragment();
        HomeActivity.getInstance().callFragment(fragment);
    }
}
