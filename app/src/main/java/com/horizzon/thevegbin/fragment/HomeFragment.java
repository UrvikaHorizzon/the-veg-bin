package com.horizzon.thevegbin.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.PagerAdapter;

import com.bumptech.glide.Glide;
import com.horizzon.thevegbin.R;
import com.horizzon.thevegbin.activity.HomeActivity;
import com.horizzon.thevegbin.activity.ItemDetailsActivity;
import com.horizzon.thevegbin.adepter.CategoryAdp;
import com.horizzon.thevegbin.adepter.ReletedItemAdp;
import com.horizzon.thevegbin.adepter.ReletedItemDaynamicAdp;
import com.horizzon.thevegbin.model.BannerItem;
import com.horizzon.thevegbin.model.BannerOffersItem;
import com.horizzon.thevegbin.model.CatItem;
import com.horizzon.thevegbin.model.DynamicData;
import com.horizzon.thevegbin.model.Home;
import com.horizzon.thevegbin.model.ProductItem;
import com.horizzon.thevegbin.model.SubCategory;
import com.horizzon.thevegbin.model.User;
import com.horizzon.thevegbin.retrofit.APIClient;
import com.horizzon.thevegbin.retrofit.GetResult;
import com.horizzon.thevegbin.utils.AutoScrollViewPager;
import com.horizzon.thevegbin.utils.SessionManager;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.tbuonomo.viewpagerdotsindicator.SpringDotsIndicator;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Call;

import static com.horizzon.thevegbin.activity.HomeActivity.homeActivity;
import static com.horizzon.thevegbin.activity.HomeActivity.txtNoti;
import static com.horizzon.thevegbin.utils.SessionManager.ABOUT_US;
import static com.horizzon.thevegbin.utils.SessionManager.CONTACT_US;
import static com.horizzon.thevegbin.utils.SessionManager.CURRUNCY;
import static com.horizzon.thevegbin.utils.SessionManager.ISCART;
import static com.horizzon.thevegbin.utils.SessionManager.O_MIN;
import static com.horizzon.thevegbin.utils.SessionManager.PRIVACY;
import static com.horizzon.thevegbin.utils.SessionManager.RAZ_KEY;
import static com.horizzon.thevegbin.utils.SessionManager.SEND_SMSNO;
import static com.horizzon.thevegbin.utils.SessionManager.TAX;
import static com.horizzon.thevegbin.utils.SessionManager.TREMSCODITION;
import static com.horizzon.thevegbin.utils.Utiles.ProductDatum;


public class HomeFragment extends Fragment implements CategoryAdp.RecyclerTouchListener, ReletedItemAdp.ItemClickListener, GetResult.MyListener, ReletedItemDaynamicAdp.ItemClickListener {
    @BindView(R.id.viewPager)
    AutoScrollViewPager viewPager;
    /*@BindView(R.id.tabview)
    TabLayout tabview;*/
    @BindView(R.id.viewPager_two)
    AutoScrollViewPager viewPager_two;
    @BindView(R.id.tabview_two)
    SpringDotsIndicator tabview_two;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    //  @BindView(R.id.recycler_releted)
    //   RecyclerView recyclerReleted;
    @BindView(R.id.lvl_selected)
    LinearLayout lvlSelected;
    @BindView(R.id.ed_search)
    EditText edSearch;
    @BindView(R.id.img_search)
    ImageView imgSearch;
    @BindView(R.id.ed_cate_search)
    EditText cate_Search;
    Unbinder unbinder;
    private Context mContext;
    CategoryAdp adapter;
    ReletedItemAdp adapterReletedi;
    List<CatItem> categoryList;
    List<BannerItem> bannerDatumList;
    List<BannerOffersItem.Datum> banneroffersList;
    public  HomeFragment HomeListFragment;
    SessionManager sessionManager;
    User user;
    List<DynamicData> dynamicDataList = new ArrayList<>();
    ReletedItemAdp reletedItemAdp;
    Fragment fragment1 = null;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        unbinder = ButterKnife.bind(this, view);
        bannerDatumList = new ArrayList<>();
        banneroffersList = new ArrayList<>();
        sessionManager = new SessionManager(mContext);
        HomeListFragment = this;
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        LinearLayoutManager mLayoutManager1 = new LinearLayoutManager(mContext);
        mLayoutManager1.setOrientation(LinearLayoutManager.HORIZONTAL);
        // recyclerReleted.setLayoutManager(mLayoutManager1);
        categoryList = new ArrayList<>();


        adapter = new CategoryAdp(mContext, categoryList, this);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
        //   adapterReletedi = new ReletedItemAdp(mContext, ProductDatum, this);
        //   recyclerReleted.setItemAnimator(new DefaultItemAnimator());
        //  recyclerReleted.setAdapter(adapterReletedi);
        user = sessionManager.getUserDetails("");
        getHome();
     //   getHome_Banner();
        addTextWatcher();
        return view;
    }

    public EditText passThisEditText() {
        return edSearch;
    }

    private void addTextWatcher() {
        edSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (edSearch.getText().toString().trim().length() == 0) {
//                    fragment1 = null;
                }
            }
        });
    }

    private void setJoinPlayrList(LinearLayout lnrView, List<DynamicData> dataList) {

        lnrView.removeAllViews();
        for (int i = 0; i < dataList.size(); i++) {
            LayoutInflater inflater = LayoutInflater.from(getActivity());
            View view = inflater.inflate(R.layout.list_home_item, null);
            TextView itemTitle = view.findViewById(R.id.itemTitle);
            RecyclerView recycler_view_list = view.findViewById(R.id.recycler_view_list);
            itemTitle.setText(dataList.get(i).getTitle());
            ReletedItemDaynamicAdp itemAdp = new ReletedItemDaynamicAdp(mContext, dataList.get(i).getDynamicItems(), this);
            recycler_view_list.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
            recycler_view_list.setAdapter(itemAdp);
            lnrView.addView(view);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onClickItem(String titel, int position) {
        homeActivity.ShowMenu();
        Bundle args = new Bundle();
        args.putInt("id", position);
        args.putString("titel", titel);
        Fragment fragment = new SubCategoryFragment();
        fragment.setArguments(args);
        HomeActivity.getInstance().callFragment(fragment);
    }
    @Override
    public void onLongClickItem(View v, int position) {
        Log.e("posiotn",""+position);
    }
    @Override
    public void onItemClick(ProductItem productItem, int position) {
        mContext.startActivity(new Intent(mContext, ItemDetailsActivity.class).putExtra("MyClass", productItem).putParcelableArrayListExtra("MyList", productItem.getPrice()));
    }
    @OnClick({R.id.txt_viewll, /*R.id.txt_viewllproduct,*/ R.id.img_search,R.id.ed_cate_search})
    public void onClick(View view) {
        Bundle args;
        switch (view.getId()) {
            case R.id.txt_viewll:
                CategoryFragment fragment = new CategoryFragment();
                Bundle bundle = new Bundle();
                bundle.putSerializable("arraylist", (Serializable) categoryList);
                fragment.setArguments(bundle);
                HomeActivity.getInstance().callFragment(fragment);
                break;
           /* case R.id.txt_viewllproduct:
                PopularFragment fragmentp = new PopularFragment();
                HomeActivity.getInstance().callFragment(fragmentp);
                break;*/
            case R.id.img_search:
                if (edSearch.getText().toString().trim().length() != 0) {
                    Fragment fragment_search;
                    args = new Bundle();
                    args.putInt("id", 0);
                    args.putString("search", edSearch.getText().toString().trim());
                    fragment_search = new ItemListFragment();
                    fragment_search.setArguments(args);
                    HomeActivity.getInstance().callFragment(fragment_search);
                } else {
                    fragment1 = null;
                }
                break;
            case R.id.ed_cate_search:
                CategoryFragment fragment_cat = new CategoryFragment();
                Bundle bundle_cat = new Bundle();
                bundle_cat.putSerializable("arraylist", (Serializable) categoryList);
                fragment_cat.setArguments(bundle_cat);
                HomeActivity.getInstance().callFragment(fragment_cat);
                break;
            default:

                break;
        }
    }


    public class MyCustomPagerAdapter extends PagerAdapter {
        Context context;
        List<BannerItem> bannerDatumList;
        LayoutInflater layoutInflater;

        public MyCustomPagerAdapter(Context context, List<BannerItem> bannerDatumList) {
            this.context = context;
            this.bannerDatumList = bannerDatumList;
            layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }
        @Override
        public int getCount() {
            return bannerDatumList.size();
        }
        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == ((LinearLayout) object);
        }
        @Override
        public Object instantiateItem(ViewGroup container, final int position) {
            View itemView = layoutInflater.inflate(R.layout.item_banner, container, false);
            ImageView imageView = (ImageView) itemView.findViewById(R.id.imageView);
            Glide.with(mContext).load(APIClient.Base_URL + "/" + bannerDatumList.get(position).getBimg()).placeholder(R.drawable.empty).into(imageView);
            container.addView(itemView);
            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!bannerDatumList.get(position).getCid().equalsIgnoreCase("0")) {
                        homeActivity.ShowMenu();
                        Bundle args = new Bundle();
                        args.putInt("id", Integer.parseInt(bannerDatumList.get(position).getCid()));
                        Fragment fragment = new SubCategoryFragment();
                        fragment.setArguments(args);
                        HomeActivity.getInstance().callFragment(fragment);
                    }
                }
            });
            return itemView;
        }
        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((LinearLayout) object);
        }
    }
    public class MyCustomPagerAdapter_two extends PagerAdapter {
        Context context;
        List<BannerOffersItem.Datum> banneroffersList;
        LayoutInflater layoutInflater;

        public MyCustomPagerAdapter_two(Context context, List<BannerOffersItem.Datum> banneroffersList) {
            this.context = context;
            this.banneroffersList = banneroffersList;
            layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }
        @Override
        public int getCount() {
            return banneroffersList.size();
        }
        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == ((LinearLayout) object);
        }
        @Override
        public Object instantiateItem(ViewGroup container, final int position) {
            View itemView = layoutInflater.inflate(R.layout.item_offers_banner, container, false);
            ImageView imageView1 = (ImageView) itemView.findViewById(R.id.offers_imageView);
            Glide.with(mContext).load(APIClient.Base_URL + "/" + banneroffersList.get(position).getBannerImage());
            container.addView(itemView);

            return itemView;
        }
        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((LinearLayout) object);
        }
    }

    private void getHome() {
        HomeActivity.custPrograssbar.PrograssCreate(getActivity());
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("uid", user.getId());
            JsonParser jsonParser = new JsonParser();
            Call<JsonObject> call = APIClient.getInterface().getHome((JsonObject) jsonParser.parse(jsonObject.toString()));
            GetResult getResult = new GetResult();
            getResult.setMyListener(this);
            getResult.callForLogin(call, "homepage");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void getHome_Banner() {
        HomeActivity.custPrograssbar.PrograssCreate(getActivity());
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("uid", user.getId());
            JsonParser jsonParser = new JsonParser();
            Call<JsonObject> call = APIClient.getInterface().getHome_banner((JsonObject) jsonParser.parse(jsonObject.toString()));
            GetResult getResult = new GetResult();
            getResult.setMyListener(this);
            getResult.callForLogin(call, "homepage");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    @Override
    public void onResume() {
        super.onResume();
        HomeActivity.getInstance().setdata();
        HomeActivity.getInstance().setFrameMargin(60);
        HomeActivity.getInstance().serchviewShow();
        if (user != null)
            HomeActivity.getInstance().titleChange("Hello " + user.getName());

        if (dynamicDataList != null) {
            setJoinPlayrList(lvlSelected, dynamicDataList);
        }
        if (reletedItemAdp != null) {
            reletedItemAdp.notifyDataSetChanged();
        }
        if (ISCART) {
            ISCART = false;
            CardFragment fragment = new CardFragment();
            HomeActivity.getInstance().callFragment(fragment);
        }
        if (edSearch != null)
            edSearch.setText("");

    }
    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void  callback(JsonObject result, String callNo) {
        try {
            if(callNo.equalsIgnoreCase("homepage")){
                HomeActivity.custPrograssbar.ClosePrograssBar();
                bannerDatumList=new ArrayList<>();
                banneroffersList=new ArrayList<>();
                categoryList=new ArrayList<>();
                Gson gson = new Gson();
                Home home = gson.fromJson(result.toString(), Home.class);
                CatItem catItem =new CatItem();
                catItem.setId("0");
                catItem.setCatname("Dairy Product");
                catItem.setCatimg(catItem.getCatimg());
                categoryList.add(catItem);
                categoryList.addAll(home.getResultHome().getCatItems());
                adapter = new CategoryAdp(mContext, categoryList, this);
                recyclerView.setAdapter(adapter);

                bannerDatumList.addAll(home.getResultHome().getBannerItems());
                MyCustomPagerAdapter myCustomPagerAdapter = new MyCustomPagerAdapter(mContext, bannerDatumList);
                viewPager.setAdapter(myCustomPagerAdapter);
                viewPager.startAutoScroll();
                viewPager.setInterval(3000);
                viewPager.setCycle(true);
                viewPager.setStopScrollWhenTouch(true);

            //    BannerOffersItem home2 = gson.fromJson(result.toString(), BannerOffersItem.class);
              //  banneroffersList = home2.getData();
              //  if (banneroffersList.get(0).getBannerImage() !=null) {
                viewPager_two.setAdapter(myCustomPagerAdapter);
                viewPager_two.startAutoScroll();
                viewPager_two.setInterval(3000);
                viewPager_two.setCycle(true);
                viewPager_two.setStopScrollWhenTouch(true);
                tabview_two.setViewPager(viewPager_two);
              //  }
                reletedItemAdp = new ReletedItemAdp(mContext, home.getResultHome().getProductItems(), this);
                //  recyclerReleted.setAdapter(reletedItemAdp);
                if (home.getResultHome().getRemainNotification() <= 0) {
                    txtNoti.setVisibility(View.GONE);
                } else {
                    txtNoti.setVisibility(View.VISIBLE);
                    txtNoti.setText("" + home.getResultHome().getRemainNotification());
                }
                sessionManager.setStringData(CURRUNCY, home.getResultHome().getMain_Data().getCurrency());
                sessionManager.setStringData(PRIVACY, home.getResultHome().getMain_Data().getPrivacy_policy());
                sessionManager.setStringData(ABOUT_US, home.getResultHome().getMain_Data().getAbout_us());
                sessionManager.setStringData(CONTACT_US, home.getResultHome().getMain_Data().getContact_us());
                sessionManager.setStringData(TREMSCODITION, home.getResultHome().getMain_Data().getTerms());
                sessionManager.setIntData(O_MIN, home.getResultHome().getMain_Data().getO_min());
                sessionManager.setStringData(RAZ_KEY, home.getResultHome().getMain_Data().getRaz_key());
                sessionManager.setStringData(TAX, home.getResultHome().getMain_Data().getTax());
                sessionManager.setStringData(SEND_SMSNO, home.getResultHome().getMain_Data().getSMSMobile());

                ProductDatum = home.getResultHome().getProductItems();
                dynamicDataList = home.getResultHome().getDynamicData();
                setJoinPlayrList(lvlSelected, dynamicDataList);
            }

        } catch (Exception e) {
            e.toString();
        }
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }
}
