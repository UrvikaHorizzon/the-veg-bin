package com.horizzon.thevegbin.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.horizzon.thevegbin.R;
import com.horizzon.thevegbin.activity.HomeActivity;
import com.horizzon.thevegbin.adepter.SubCategoryAdp;
import com.horizzon.thevegbin.model.CatItem;
import com.horizzon.thevegbin.model.SubCategory;
import com.horizzon.thevegbin.model.SubcatItem;
import com.horizzon.thevegbin.model.User;
import com.horizzon.thevegbin.retrofit.APIClient;
import com.horizzon.thevegbin.retrofit.GetResult;
import com.horizzon.thevegbin.utils.SessionManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;

import static com.horizzon.thevegbin.activity.HomeActivity.homeActivity;

public class SubCategoryFragment extends Fragment implements GetResult.MyListener, SubCategoryAdp.RecyclerTouchListener {
    @BindView(R.id.lvl_notfound)
    LinearLayout lvlNotfound;
    @BindView(R.id.subCategory_bannerr)
    ImageView sab_banner;
    @BindView(R.id.txt_notfound)
    TextView txtNotfound;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.txt_titel)
    TextView txtTitel;
    SubCategoryAdp adapter;
    List<SubcatItem> categoryList;
    Unbinder unbinder;
    SessionManager sessionManager;
    User user;
    int CID = 0;
    int position;

    // public static HashMap<String,SubcatItem> contactItems=new HashMap<String, SubcatItem>();
    public SubCategoryFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static SubCategoryFragment newInstance(String param1, String param2) {
        SubCategoryFragment fragment = new SubCategoryFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_subcategory, container, false);
        unbinder = ButterKnife.bind(this, view);
        Bundle b = getArguments();
        CID = b.getInt("id");
        String titel = b.getString("titel");
        if (titel != null) {
            txtTitel.setText("" + titel);
        } else {
            txtTitel.setVisibility(View.GONE);
        }


        HomeActivity.getInstance().setFrameMargin(60);
        categoryList = new ArrayList<>();
        sessionManager = new SessionManager(getActivity());
        user = sessionManager.getUserDetails("");
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        getSubCategory();
        return view;
    }

    @Override
    public void onClickItem(View v, int cid, int scid) {
        homeActivity.ShowMenu();
        Bundle args = new Bundle();
        args.putInt("cid", cid);
        args.putInt("scid", scid);
        Fragment fragment = new ItemListFragment();
        fragment.setArguments(args);
        HomeActivity.getInstance().callFragment(fragment);
    }

    @Override
    public void onLongClickItem(View v, int position) {
    }

    private void getSubCategory() {
        HomeActivity.custPrograssbar.PrograssCreate(getActivity());
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("category_id", CID);
            JsonParser jsonParser = new JsonParser();
            Call<JsonObject> call = APIClient.getInterface().getSubcategory((JsonObject) jsonParser.parse(jsonObject.toString()));
            GetResult getResult = new GetResult();
            getResult.setMyListener(this);
            getResult.callForLogin(call, "1");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void callback(JsonObject result, String callNo) {
        try {
            HomeActivity.custPrograssbar.ClosePrograssBar();
            if (callNo.equalsIgnoreCase("1") && result.toString() != null) {
                Gson gson = new Gson();
                SubCategory category = gson.fromJson(result.toString(), SubCategory.class);
                if (category.getResult().equalsIgnoreCase("true")) {
                    categoryList = category.getData();
                    if (categoryList.size() != 0) {
                        adapter = new SubCategoryAdp(getActivity(), categoryList, this);
                        recyclerView.setAdapter(adapter);
                    } else {
                        lvlNotfound.setVisibility(View.VISIBLE);
                        txtNotfound.setText("" + category.getResponseMsg());
                    }
                } else {
                    lvlNotfound.setVisibility(View.VISIBLE);
                    txtNotfound.setText("" + category.getResponseMsg());
                }
                for (int i = 0; i < categoryList.size(); i++) {

                    if (categoryList.get(i).getCategory_banner() !=null) {
                        sab_banner.setVisibility(View.VISIBLE);
                        categoryList.get(i).getCategory_banner();
                        SubcatItem category1 = categoryList.get(i);
                        SubcatItem myCategory = new SubcatItem();
                        myCategory.setCategory_banner(category1.getCategory_banner());
                        Glide.with(getActivity()).load(APIClient.Base_URL + "/" + category1.getCategory_banner()).thumbnail(Glide.with(getActivity()).load("")).into(sab_banner);
                    } else {
                        sab_banner.setVisibility(View.GONE);
                    }
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        HomeActivity.getInstance().serchviewShow();
        HomeActivity.getInstance().setFrameMargin(60);
        if (user != null)
            HomeActivity.getInstance().titleChange("Hello " + user.getName());

    }
}
