package com.horizzon.thevegbin.fragment;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.horizzon.thevegbin.MainActivity;
import com.horizzon.thevegbin.R;
import com.horizzon.thevegbin.activity.HomeActivity;
import com.horizzon.thevegbin.activity.ItemDetailsActivity;
import com.horizzon.thevegbin.adepter.DairyProductAdp;
import com.horizzon.thevegbin.database.DatabaseHelper;
import com.horizzon.thevegbin.model.DairyItem;
import com.horizzon.thevegbin.model.PaymentItem;
import com.horizzon.thevegbin.model.ProductItem;
import com.horizzon.thevegbin.retrofit.APIClient;
import com.horizzon.thevegbin.utils.SessionManager;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class SubscribeFragment extends Fragment {

    ProductItem productItem;
    @BindView(R.id.img_icons)
    ImageView img_product;
    @BindView(R.id.lvlbacket)
    LinearLayout lvlbacket;
    @BindView(R.id.txt_dairyitem)
    TextView txtItem;
    @BindView(R.id.txt_notfound)
    TextView txtNotfound;
    @BindView(R.id.txt_dairyprice)
    TextView txtPrice;
    @BindView(R.id.lvl_notfound)
    LinearLayout lvlNotfound;
    @BindView(R.id.subscribe_recycler)
    RecyclerView rvsubscribelist;

    @BindView(R.id.first)
    Button first_cal;
    @BindView(R.id.second)
    Button second_cal;
    @BindView(R.id.third)
    Button third_cal;
    @BindView(R.id.txt_sub_start_Date)
    TextView txtdate;

    List<DairyItem> dairyDataList;
    DairyProductAdp itemAdp;

    Unbinder unbinder;
    SessionManager sessionManager;
    DatabaseHelper databaseHelper;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_subscribe, container, false);
        HomeActivity.getInstance().setFrameMargin(60);
        unbinder = ButterKnife.bind(this, view);
        databaseHelper = new DatabaseHelper(getActivity());
        sessionManager = new SessionManager(getActivity());

        List<String> myList = new ArrayList<String>();
        myList.add(productItem.getProductImage());
        Glide.with(getActivity()).load(APIClient.Base_URL + "/" + productItem.getProductRelatedImage()).placeholder(R.drawable.empty).into(img_product);

        return view;

    }

    @OnClick({R.id.first, R.id.second, R.id.third})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.first:
                openDatePickerDialog();
                break;
            case R.id.second:
                setDateField_s();
                break;
            case R.id.third:
                setDateField_t();
                break;
            default:
        }
    }

    public void openDatePickerDialog() {
        final Calendar c = Calendar.getInstance();

        int y = c.get(Calendar.YEAR);
        int m = c.get(Calendar.MONTH);
        int d = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog dp = new DatePickerDialog(getActivity(),
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        String erg = "";
                        erg = String.valueOf(dayOfMonth);
                        erg += "." + String.valueOf(monthOfYear + 1);
                        erg += "." + year;

                        SimpleDateFormat simpledateformat = new SimpleDateFormat("EEEE");
                        Date date = new Date(year, monthOfYear, dayOfMonth-1);
                        String dayOfWeek = simpledateformat.format(date);

                        txtdate.setText(dayOfWeek + "," + erg);

                    }

                }, y, m, d);
        dp.show();
    }


    private void setDateField_s() {
        final Calendar c = Calendar.getInstance();

        int y = c.get(Calendar.YEAR);
        int m = c.get(Calendar.MONTH);
        int d = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog dp = new DatePickerDialog(getActivity(),
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        String erg = "";
                        erg = String.valueOf(dayOfMonth);
                        erg += "." + String.valueOf(monthOfYear + 1);
                        erg += "." + year;

                        SimpleDateFormat simpledateformat = new SimpleDateFormat("EEEE");
                        Date date = new Date(year, monthOfYear, dayOfMonth-1);
                        String dayOfWeek = simpledateformat.format(date);

                        txtdate.setText(dayOfWeek + "," + erg);

                    }

                }, y, m, d);
        dp.show();

    }

    private void setDateField_t() {
        final Calendar c = Calendar.getInstance();

        int y = c.get(Calendar.YEAR);
        int m = c.get(Calendar.MONTH);
        int d = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog dp = new DatePickerDialog(getActivity(),
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        String erg = "";
                        erg = String.valueOf(dayOfMonth);
                        erg += "." + String.valueOf(monthOfYear + 1);
                        erg += "." + year;

                        SimpleDateFormat simpledateformat = new SimpleDateFormat("EEEE");
                        Date date = new Date(year, monthOfYear, dayOfMonth-1);
                        String dayOfWeek = simpledateformat.format(date);

                        txtdate.setText(dayOfWeek + "," + erg);

                    }

                }, y, m, d);
        dp.show();

    }
}
