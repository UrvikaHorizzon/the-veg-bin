package com.horizzon.thevegbin.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.horizzon.thevegbin.R;
import com.horizzon.thevegbin.activity.HomeActivity;
import com.horizzon.thevegbin.adepter.HistoryAdapter;
import com.horizzon.thevegbin.adepter.SubscribeListAdapter;
import com.horizzon.thevegbin.model.SubscribHistory_Response;
import com.horizzon.thevegbin.model.Subscribe_DataList;
import com.horizzon.thevegbin.model.User;
import com.horizzon.thevegbin.retrofit.APIClient;
import com.horizzon.thevegbin.utils.CustPrograssbar;
import com.horizzon.thevegbin.utils.SessionManager;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SubscriptionListFragment extends Fragment {

    @BindView(R.id.lvl_mysubscrib)
    RecyclerView lvlMysub;
    Unbinder unbinder;
    @BindView(R.id.txt_notfound)
    TextView txtNotfound;
    @BindView(R.id.lvl_notfound)
    LinearLayout lvlNotfound;

    List<Subscribe_DataList> subscribhistoryList = new ArrayList<>();
    SubscribeListAdapter Subscribehistoryadapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    SessionManager sessionManager;
    User user;
    CustPrograssbar custPrograssbar;
    String rs;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_subscription_list, container, false);
        custPrograssbar = new CustPrograssbar();
        sessionManager = new SessionManager(getActivity());
        user = sessionManager.getUserDetails("");
        unbinder = ButterKnife.bind(this, view);
        HomeActivity.getInstance().setFrameMargin(0);
        rs = user.getId();
        getSubscribhistory(rs);

        lvlMysub.setLayoutManager(new GridLayoutManager(getActivity(), 1));
        LinearLayoutManager mLayoutManager1 = new LinearLayoutManager(getActivity());
        mLayoutManager1.setOrientation(LinearLayoutManager.HORIZONTAL);

        return view;
    }

    private void getSubscribhistory(String id) {

        Call<SubscribHistory_Response> call = APIClient.getInterface().getSubscribHistory(id);
        call.enqueue(new Callback<SubscribHistory_Response>() {
            @Override
            public void onResponse(Call<SubscribHistory_Response> call, Response<SubscribHistory_Response> response) {
                if (response.isSuccessful()) {


                    if (response.body().getResponseCode().equals("200")) {
                       /* historyList.addAll(response.body().getDetail());
                        rvrecycle.setAdapter(historyadapter);*/
                      //  if (subscribhistoryList.size() != 0) {
                            SubscribHistory_Response jsonResponse = response.body();
                            subscribhistoryList = new ArrayList<Subscribe_DataList>(jsonResponse.getData());
                            Subscribehistoryadapter = new SubscribeListAdapter(getActivity(),subscribhistoryList);
                            lvlMysub.setAdapter(Subscribehistoryadapter);
                       /* } else {
                            custPrograssbar.ClosePrograssBar();
                            lvlNotfound.setVisibility(View.VISIBLE);
                            txtNotfound.setText("You dont't any active subscription");
                        }*/


                    } else if (response.body().getResponseCode().equals("401")) {

                        Toast.makeText(getActivity(), "no Data Found", Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<SubscribHistory_Response> call, Throwable t) {

            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
