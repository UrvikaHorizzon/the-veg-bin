package com.horizzon.thevegbin.fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.horizzon.thevegbin.R;
import com.horizzon.thevegbin.activity.HomeActivity;
import com.horizzon.thevegbin.activity.MyWalletActivity;
import com.horizzon.thevegbin.database.DatabaseHelper;
import com.horizzon.thevegbin.model.PaymentItem;
import com.horizzon.thevegbin.model.User;
import com.horizzon.thevegbin.model.WalletRespone_Data;
import com.horizzon.thevegbin.model.detail;
import com.horizzon.thevegbin.retrofit.APIClient;
import com.horizzon.thevegbin.utils.CustPrograssbar;
import com.horizzon.thevegbin.utils.FragmentCallback;
import com.horizzon.thevegbin.utils.SessionManager;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WalletFragment extends Fragment implements View.OnClickListener {

    Unbinder unbinder;
    CustPrograssbar custPrograssbar;
    private FragmentCallback fragmentCallback;
    OrderSumrryFragment fragment;
    private double total_price;
    @BindView(R.id.wallet_txtcount)
    TextView available_wallet_amount;
    @BindView(R.id.wallet_apply_btn)
    Button walletamount_apply_btn;
    private String TIME;
    private String DATA;
    private String PAYMENT;
    PaymentItem paymentItem;
    List<detail> walletlist;

    DatabaseHelper databaseHelper;
    SessionManager sessionManager;
    User user;
    String rs;
    String value;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.wallet_layout, container, false);
        unbinder = ButterKnife.bind(this, view);
        custPrograssbar = new CustPrograssbar();
        fragment = new OrderSumrryFragment();
        walletamount_apply_btn.setOnClickListener(this);
        databaseHelper = new DatabaseHelper(getActivity());
        sessionManager = new SessionManager(getActivity());
        user = sessionManager.getUserDetails("");
      //  available_wallet_amount.setText("\u20B9 " + user.getWallet());
        rs = user.getId();
        getWalletUse(rs);

        if (getArguments() != null) {
            TIME = getArguments().getString("TIME");
            DATA = getArguments().getString("DATE");
            PAYMENT = getArguments().getString("PAYMENT");
            Log.d("TAG", "onCreate: " + PAYMENT);
            paymentItem = (PaymentItem) getArguments().getSerializable("PAYMENTDETAILS");
        }
        //  fragment.setFragmentCallback(this);

        return view;

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onResume() {
        super.onResume();
        HomeActivity.getInstance().serchviewHide();
        HomeActivity.getInstance().setFrameMargin(0);

    }

    private void getWalletUse(String id) {

        Call<WalletRespone_Data> call = APIClient.getInterface().getWalletTotal(id);
        call.enqueue(new Callback<WalletRespone_Data>() {
            @Override
            public void onResponse(Call<WalletRespone_Data> call, Response<WalletRespone_Data> response) {
                if (response.isSuccessful()) {


                    if (response.body().getResponseCode().equals("200")) {

                        walletlist = response.body().getDetail();
                        detail myCategory = new detail();
                        for (int i = 0; i < walletlist.size(); i++) {
                            available_wallet_amount.setText("\u20B9 " + walletlist.get(i).getWalletAmount());

                            value = walletlist.get(i).getWalletAmount();

                            walletamount_apply_btn.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    getWalletValue(value);
                                }
                            });
                        }
                    } else if (response.body().getResponseCode().equals("401")) {
                        //citylist.clear();
                        // walletlist = new ArrayList<>();
                        // getAreaList(state_id, city_id);
                        Toast.makeText(getActivity(), "no Data Found", Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<WalletRespone_Data> call, Throwable t) {

            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

          /*  case R.id.wallet_apply_btn:
                custPrograssbar.PrograssCreate(getActivity());
                getWalletValue();
                sessionManager.ApplyPromoCode(true);
                break;*/

        }
    }

    private void getWalletValue(String value) {
        User user = sessionManager.getUserDetails("");

        int total_walletamount = Integer.parseInt(value);
        Log.d("TAG", "getWalletValue: " + total_walletamount);


        OrderSumrryFragment fragment2 = new OrderSumrryFragment();
        Bundle bundle1 = new Bundle();
        bundle1.putString("DATE", DATA);
        bundle1.putString("TIME", TIME);
        bundle1.putString("PAYMENT", PAYMENT);
        double data = total_walletamount;
        bundle1.putDouble("lat", data);
        bundle1.putSerializable("PAYMENTDETAILS", paymentItem);
        fragment2.setArguments(bundle1);
        fragmentCallback.onDataSents(data);
        HomeActivity.getInstance().callFragment(fragment2);

    }

    public void setFragmentCallback(FragmentCallback callback) {
        this.fragmentCallback = callback;
    }


}
