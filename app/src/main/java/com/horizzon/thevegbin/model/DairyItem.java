package com.horizzon.thevegbin.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("unused")
public class DairyItem implements Parcelable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("product_name")
    @Expose
    private String productName;
    @SerializedName("seller_name")
    @Expose
    private String sellerName;
    @SerializedName("short_desc")
    @Expose
    private String shortDesc;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("product_image")
    @Expose
    private String productImage;
    @SerializedName("product_related_image")
    @Expose
    private String productRelatedImage;
    @SerializedName("price")
    @Expose
    private ArrayList<Price> price = null;
    @SerializedName("stock")
    @Expose
    private String stock;
    @SerializedName("no_of_stock")
    @Expose
    private int noOfStock;
    @SerializedName("discount")
    @Expose
    private int discount;
    @SerializedName("popular")
    @Expose
    private String popular;
    @SerializedName("subscribe")
    @Expose
    private String subscribe;

    protected DairyItem(Parcel in) {
        id = in.readString();
        productName = in.readString();
        sellerName = in.readString();
        shortDesc = in.readString();
        status = in.readString();
        productImage = in.readString();
        productRelatedImage = in.readString();
        stock = in.readString();
        noOfStock = in.readInt();
        discount = in.readInt();
        popular = in.readString();
        subscribe = in.readString();
    }


    public static final Creator<DairyItem> CREATOR = new Creator<DairyItem>() {
        @Override
        public DairyItem createFromParcel(Parcel in) {
            return new DairyItem(in);
        }

        @Override
        public DairyItem[] newArray(int size) {
            return new DairyItem[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getSellerName() {
        return sellerName;
    }

    public void setSellerName(String sellerName) {
        this.sellerName = sellerName;
    }

    public String getShortDesc() {
        return shortDesc;
    }

    public void setShortDesc(String shortDesc) {
        this.shortDesc = shortDesc;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getProductImage() {
        return productImage;
    }

    public void setProductImage(String productImage) {
        this.productImage = productImage;
    }

    public String getProductRelatedImage() {
        return productRelatedImage;
    }

    public void setProductRelatedImage(String productRelatedImage) {
        this.productRelatedImage = productRelatedImage;
    }

    public ArrayList<Price> getPrice() {
        return price;
    }

    public void setPrice(ArrayList<Price> price) {
        this.price = price;
    }

    public String getStock() {
        return stock;
    }

    public void setStock(String stock) {
        this.stock = stock;
    }

    public int getNoOfStock() {
        return noOfStock;
    }

    public void setNoOfStock(int noOfStock) {
        this.noOfStock = noOfStock;
    }

    public int getDiscount() {
        return discount;
    }

    public void setDiscount(int discount) {
        this.discount = discount;
    }

    public String getPopular() {
        return popular;
    }

    public void setPopular(String popular) {
        this.popular = popular;
    }

    public String getSubscribe() {
        return subscribe;
    }

    public void setSubscribe(String subscribe) {
        this.subscribe = subscribe;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(productName);
        dest.writeString(sellerName);
        dest.writeString(shortDesc);
        dest.writeString(status);
        dest.writeString(productImage);
        dest.writeString(productRelatedImage);
        dest.writeString(stock);
        dest.writeInt(noOfStock);
        dest.writeInt(discount);
        dest.writeString(popular);
        dest.writeString(subscribe);
    }

}
