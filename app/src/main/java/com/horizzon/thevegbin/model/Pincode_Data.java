package com.horizzon.thevegbin.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Pincode_Data {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("pincode")
    @Expose
    private String pincodename;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPncodename() {
        return pincodename;
    }

    public void setPncodename(String pincodename) {
        this.pincodename = pincodename;
    }


    @Override
    public String toString() {
        return this.pincodename; // What to display in the Spinner list.
    }
}
