package com.horizzon.thevegbin.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Response_Data implements Serializable{


        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("userid")
        @Expose
        private String userid;
        @SerializedName("amount")
        @Expose
        private String amount;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("debit_credit")
        @Expose
        private String DebitCredit;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getUserid() {
            return userid;
        }

        public void setUserid(String userid) {
            this.userid = userid;
        }

        public String getAmount() {
            return amount;
        }

        public void setAmount(String amount) {
            this.amount = amount;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getDebitCredit() {
        return DebitCredit;
        }

        public void setDebitCredit(String debitCredit) {
        this.DebitCredit = debitCredit;
        }

}