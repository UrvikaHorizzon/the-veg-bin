
package com.horizzon.thevegbin.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@SuppressWarnings("unused")
public class SubcatItem implements Serializable {

    @SerializedName("cat_id")
    private String mCatId;
    @SerializedName("id")
    private String mId;
    @SerializedName("img")
    private String mImg;
    @SerializedName("name")
    private String mName;
    @SerializedName("count")
    private String count;
    @SerializedName("category_banner")
    private String mcategory_banner;


    public String getCategory_banner() {
        return mcategory_banner;
    }

    public void setCategory_banner(String categorybanner) {
        mcategory_banner = categorybanner;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public String getCatId() {
        return mCatId;
    }

    public void setCatId(String catId) {
        mCatId = catId;
    }

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public String getImg() {
        return mImg;
    }

    public void setImg(String img) {
        mImg = img;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

}
