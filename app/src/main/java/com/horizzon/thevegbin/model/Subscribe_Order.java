package com.horizzon.thevegbin.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Subscribe_Order {

    @SerializedName("ResponseCode")
    @Expose
    private String responseCode;
    @SerializedName("detail")
    @Expose
    private Subscribe_Data detail;
    @SerializedName("Result")
    @Expose
    private String result;
    @SerializedName("ResponseMsg")
    @Expose
    private String responseMsg;

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public Subscribe_Data getDetail() {
        return detail;
    }

    public void setDetail(Subscribe_Data detail) {
        this.detail = detail;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getResponseMsg() {
        return responseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        this.responseMsg = responseMsg;
    }

    public class Subscribe_Data implements Serializable {

        @SerializedName("image_path")
        @Expose
        private String img;
        @SerializedName("product_name")
        @Expose
        private String productname;
        @SerializedName("product_price")
        @Expose
        private String productprice;
        @SerializedName("pick_scheduletime")
        @Expose
        private String picksche;
        @SerializedName("deliverytime")
        @Expose
        private String deliverytime;
        @SerializedName("address_id")
        @Expose
        private String address;

        public String getImg() {
            return img;
        }

        public void setImg(String img) {
            this.img = img;
        }

        public String getProductName() {
            return productname;
        }

        public void setProductName(String productname) {
            this.productname = productname;
        }

        public String getProductPrice() {
            return productprice;
        }

        public void setProductPrice(String productPrice) {
            this.productprice = productPrice;
        }

        public String getPickSchTime() {
            return picksche;
        }

        public void setPickSchTime(String picksche) {
            this.picksche = picksche;
        }

        public String getDeliveyTime() {
            return deliverytime;
        }

        public void setDeliveyTime(String deliveryTime) {
            this.deliverytime = deliveryTime;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

    }
}
