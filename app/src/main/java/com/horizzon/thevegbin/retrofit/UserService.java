package com.horizzon.thevegbin.retrofit;


import com.google.gson.JsonObject;
import com.horizzon.thevegbin.model.Area;
import com.horizzon.thevegbin.model.CityListResponse;
import com.horizzon.thevegbin.model.History_Response;
import com.horizzon.thevegbin.model.PincodeList_Response;
import com.horizzon.thevegbin.model.PromoCodeList_Response;
import com.horizzon.thevegbin.model.Promocode_Response;
import com.horizzon.thevegbin.model.ShippingResponse;
import com.horizzon.thevegbin.model.StateList_Response;
import com.horizzon.thevegbin.model.SubscribHistory_Response;
import com.horizzon.thevegbin.model.WalletRespone_Data;
import com.horizzon.thevegbin.model.WalletTotal;
import com.horizzon.thevegbin.model.googleMap.GoogleAPIResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface UserService {

    @POST(APIClient.APPEND_URL + "cat.php")
    Call<JsonObject> getCat(@Body JsonObject object);

    @POST(APIClient.APPEND_URL + "subcategory.php")
    Call<JsonObject> getSubcategory(@Body JsonObject object);

    @POST(APIClient.APPEND_URL + "product.php")
    Call<JsonObject> getGetProduct(@Body JsonObject object);

    @POST(APIClient.APPEND_URL + "home.php")
    Call<JsonObject> getHome(@Body JsonObject object);

    @POST(APIClient.APPEND_URL + "login.php")
    Call<JsonObject> getLogin(@Body JsonObject object);

    @POST(APIClient.APPEND_URL + "forgot.php")
    Call<JsonObject> getForgot(@Body JsonObject object);

    @POST(APIClient.APPEND_URL + "pinmatch.php")
    Call<JsonObject> getPinmatch(@Body JsonObject object);

    @POST(APIClient.APPEND_URL + "register.php")
    Call<JsonObject> getRegister(@Body JsonObject object);

    @POST(APIClient.APPEND_URL + "timeslot.php")
    Call<JsonObject> getTimeslot(@Body JsonObject object);

    @POST(APIClient.APPEND_URL + "paymentgateway.php")
    Call<JsonObject> getpaymentgateway(@Body JsonObject object);

    @POST(APIClient.APPEND_URL + "search.php")
    Call<JsonObject> getSearch(@Body JsonObject object);

    @POST(APIClient.APPEND_URL + "profile.php")
    Call<JsonObject> UpdateProfile(@Body JsonObject object);

    @POST(APIClient.APPEND_URL + "order.php")
    Call<JsonObject> Order(@Body JsonObject object);

    @POST(APIClient.APPEND_URL + "history.php")
    Call<JsonObject> getHistory(@Body JsonObject object);

    @POST(APIClient.APPEND_URL + "plist.php")
    Call<JsonObject> getPlist(@Body JsonObject object);

    @POST(APIClient.APPEND_URL + "feed.php")
    Call<JsonObject> SendFeed(@Body JsonObject object);

    @POST(APIClient.APPEND_URL + "rate.php")
    Call<JsonObject> Rates(@Body JsonObject object);

    @POST(APIClient.APPEND_URL + "status.php")
    Call<JsonObject> getStatus(@Body JsonObject object);

    @POST(APIClient.APPEND_URL + "noti.php")
    Call<JsonObject> getNoti(@Body JsonObject object);

    @POST(APIClient.APPEND_URL + "notiset.php")
    Call<JsonObject> getNotiSet(@Body JsonObject object);

    @POST(APIClient.APPEND_URL + "ocancle.php")
    Call<JsonObject> getOdercancle(@Body JsonObject object);

    @POST(APIClient.APPEND_URL + "area.php")
    Call<JsonObject> getArea(@Body JsonObject object);

    @POST(APIClient.APPEND_URL + "code.php")
    Call<JsonObject> getCode(@Body JsonObject object);

    @POST(APIClient.APPEND_URL + "n_read.php")
    Call<JsonObject> readNoti(@Body JsonObject object);

    @POST(APIClient.APPEND_URL + "alist.php")
    Call<JsonObject> getAddress(@Body JsonObject object);

    @POST(APIClient.APPEND_URL + "add_del.php")
    Call<JsonObject> deleteAddress(@Body JsonObject object);

    @POST(APIClient.APPEND_URL + "address.php")
    Call<JsonObject> UpdateAddress(@Body JsonObject object);

    @POST(APIClient.APPEND_URL + "promocode_api.php")
    Call<Promocode_Response> getPromoCode(@Query("title") String title, @Query("user_id") String user_id);

    @GET(APIClient.APPEND_URL + "shipping_api.php")
    Call<ShippingResponse> getShipping();

    @GET(APIClient.APPEND_URL + "promocodelist_api.php")
    Call<PromoCodeList_Response> getPromoCodeList();

    @POST(APIClient.APPEND_URL + "complain_api.php")
    Call<JsonObject> getComplain(@Body JsonObject object);

    @GET(APIClient.APPEND_URL + "home_banner_api.php")
    Call<JsonObject> getHome_banner(@Body JsonObject object);

    @GET(APIClient.APPEND_URL + "state_api.php")
    Call<StateList_Response> getStateList();

    @GET(APIClient.APPEND_URL + "city_api.php")
    Call<CityListResponse> getCityList(@Query("state_id") String state_id);

    @GET(APIClient.APPEND_URL + "getlocation.php")
    Call<GoogleAPIResponse> getCityBounds(@Query(value = "address", encoded = true) String address
    );

    @POST(APIClient.APPEND_URL + "wallet_recharge_api.php")
    Call<JsonObject> getwalletRecharge(@Body JsonObject object);

    @GET(APIClient.APPEND_URL + "pincode_api.php")
    Call<PincodeList_Response> getPincodelist();

    @GET(APIClient.APPEND_URL + "wallet_amount_api.php")
    Call<WalletRespone_Data> getWalletTotal(@Query("userid") String user_id);

    @GET(APIClient.APPEND_URL + "wallet_history_api.php")
    Call<History_Response> getWalletHistory(@Query("userid") String user_id);

    @POST(APIClient.APPEND_URL + "subscribe_api.php")
    Call<JsonObject> getDairyProduct(@Body JsonObject object);

    @POST(APIClient.APPEND_URL + "subscription_order.php")
    Call<JsonObject> getSubscibeOrder(@Body JsonObject object);

    @GET(APIClient.APPEND_URL + "subscription_order_history.php")
    Call<SubscribHistory_Response> getSubscribHistory(@Query("userid") String user_id);

    @POST(APIClient.APPEND_URL + "subscription_order_history_status.php")
    Call<JsonObject> getSubscribStatus(@Body JsonObject object);

    @POST(APIClient.APPEND_URL + "area_sub.php")
    Call<JsonObject> getAreaSub(@Body JsonObject object);

}
